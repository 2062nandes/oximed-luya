<?php

namespace app\assets;

class ResourcesAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/resources';

    public $js = [
      //  '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'js/jquery1.12.1.min.js',
        'js/jquery.easing.1.3.js',
        'js/bootstrap.min.js',
        'js/edslider.js',
        'js/helper.min.js',
        'js/main.js',
        'js/js.js',
    ];

    public $css = [
        'css/bootstrap.min.css',
  //   '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
  //   '//fonts.googleapis.com/css?family=Open+Sans:300,400,600',
  //   '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        'css/style.css',
        'css/animate.css',
        'css/buble.css',
        'css/edslider.css',
        'css/responsivo.css',
        'fontaw/font-awesome4.5.0.min.css',
    ];

    public $publishOptions = [
        'only' => [
            'css/*',
            'js/*',
            'fontaw/*',
            'css/fonts/*',
            'img/*',
            'images/*',
        ]
    ];

    public $depends = [
      //  'yii\web\JqueryAsset',
    ];
}
