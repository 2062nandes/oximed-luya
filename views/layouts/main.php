<?php
use app\assets\ResourcesAsset;
use luya\helpers\Url;

ResourcesAsset::register($this);

/* @var $this luya\web\View */
/* @var $content string */

$this->beginPage();
?>
<html lang="<?= Yii::$app->composition->language; ?>">
    <head>
        <meta charset="UTF-8" />
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>OXIMED &mdash; <?php echo $this->title; ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <header>
      <div class="container">
          <div id="content">
              <div class="title"></div>
                  <div class="navigation" id="nav">
                              <div class="item oxigeno-medicinal animated bounceInDown">
                                  <img src="images/bg_user.png" alt="" width="199" height="199" class="circle"/>
                                  <a href="oxigeno-medicinal" class="icon animated infinite pulse">
                                    <h2>OXÍGENO MEDICINAL</h2>
                                  </a>
                              </div>
                              <div class=" item oxigeno-industrial animated bounceInDown">
                                  <img src="images/bg_home.png" alt="" width="199" height="199" class="circle"/>
                                  <a href="oxigeno-industrial" class="icon animated infinite pulse">
                                    <h2>OXÍGENO INDUSTRIAL</h2>
                                  </a>
                              </div>
                              <div class="item extintores animated bounceInDown">
                                  <img src="images/bg_camera.png" alt="" width="199" height="199" class="circle"/>
                                  <a href="extintores" class="icon animated infinite pulse">
                                    <h2>EXTINTORES</h2>
                                  </a>
                              </div>
                              <div class="item equipos-ortopedicos animated bounceInDown">
                                  <img src="images/bg_fav.png" alt="" width="199" height="199" class="circle"/>
                                  <a href="equipos-ortopedicos" class="icon animated infinite pulse">
                                    <h2>EQUIPOS ORTOPÉDICOS</h2>
                                  </a>
                              </div>
                              <div class="item accesorios animated bounceInDown">
                                  <img src="images/bg_fav.png" alt="" width="199" height="199" class="circle"/>
                                  <a href="accesorios" class="icon animated infinite pulse">
                                    <h2>ACCESORIOS</h2>
                                  </a>
                              </div>
                              <div class="item servicios animated bounceInDown">
                                  <img src="images/bg_shop.png" alt="" width="199" height="199" class="circle"/>
                                  <a href="alquiler-de-equipos" class="icon animated infinite pulse">
                                    <h2>ALQUILER DE EQUIPOS</h2>
                                  </a>
                              </div>
                              <div class="item novedades-y-ofertas animated bounceInDown">
                                  <img src="images/bg_fav.png" alt="" width="199" height="199" class="circle"/>
                                  <a href="novedades-y-ofertas" class="icon animated infinite pulse">
                                    <h2>NOVEDADES Y OFERTAS</h2>
                                  </a>
                              </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                      <div class="col-sm-4 col-lg-4 col-xs-12" style="float:right">
                        <div class="topnav">
                            <div class="freewrapper">
                                <div class="btn-group btn-group-justified">
                                  <a class="col33" href="<?= $this->publicHtml; ?>/">Inicio</a>
                                  <a class="col33" href="nosotros">Nosotros</a>
                                  <a class="col33" href="contactos">Contactos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                                    <!--<div class="line"></div>-->
               </div>
              <div class="silla">
                <img class="img-responsive animated bounceInLeft center-block" width="350" src="img/silla.png" alt="silla"/>
                <p class="text-center animated zoomInUp">OXIGENO MEDICINAL DOMICILIARIO</p>
              </div>
              <div class="vibora">
                <img class="img-responsive center-block rubberBand" src="img/logo.png" width="350" alt="logo-oximed"/>
                <img class="img-responsive animated bounceInRight center-block fadeInRight" width="350" src="img/serpiente.png" alt="serpiente"/>
              </div>
            </div>
      </div>
      <!--Menu Responsivo-->
      <nav id="mainNav" class="navbar navbar-default navbar-custom">
          <div class="container">
            <div class="row">
              <div class="navbar-header center-block">
                <button type="button" class="navbar-toggle btn-block" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span> Menú <i class="fa fa-bars"></i>
                </button>
              </div>
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                      <li>
                        <a href="alquiler-de-equipos">Alquiler de equipos</a>
                      </li>
                      <li>
                        <a href="oxigeno-medicinal">Oxígeno medicinal</a>
                      </li>
                      <li>
                        <a href="oxigeno-industrial">Oxígeno industrial</a>
                      </li>
                      <li>
                        <a href="accesorios">Accesorios</a>
                      </li>
                      <li>
                        <a href="equipos-ortopedicos">Equipos ortopédicos</a>
                      </li>
                      <li>
                        <a href="novedades-y-ofertas">Novedades y ofertas</a>
                      </li>
                      <li>
                        <a href="extintores">Extintores</a>
                      </li>
                  </ul>
              </div><!-- /.navbar-collapse -->
          </div>
        </div><!-- /.container-fluid -->
      </nav>
    </header>
<!--
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="https://luya.io" target="_blank" >
                    <img alt="Brand" src="<?= $this->publicHtml; ?>/images/luya_logo_flat_icon.png" height="20px">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                <?php foreach (Yii::$app->menu->find()->where(['parent_nav_id' => 0, 'container' => 'default'])->all() as $item): ?>
                    <li <?php if ($item->isActive): ?>class="active"<?php endif;?>>
                        <a href="<?= $item->link; ?>"><?= $item->title; ?></a>
                    </li>
                <?php endforeach; ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="https://github.com/luyadev/luya" target="_blank"><i class="fa fa-github"></i></a></li>
                    <li><a href="https://twitter.com/luyadev" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCfGs4sHk-D3swX0mhxv98RA" target="_blank"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <?= Yii::t('app', 'kickstarter_success'); ?>
    <?= Yii::t('app', 'kickstarter_admin_link', ['link' => Url::toInternal(['admin/default/index']), true]); ?>
    <div class="col-md-12">
    <ol class="breadcrumb">
    <?php foreach (Yii::$app->menu->current->teardown as $item): ?>
    <li><a href="<?= $item->link; ?>"><?= $item->title; ?></a>
  <?php endforeach; ?>
</ol>
</div>
<ul>
  <?php foreach (Yii::$app->menu->findAll(['parent_nav_id' => 0]) as $item): ?>
      <li><a href="<?= $item->link;?>"><?= $item->title; ?></a>
  <?php endforeach; ?>
</ul>
-->
<div>
        <?php if (count(Yii::$app->menu->getLevelContainer(2)) > 0): ?>
            <div class="col-md-3">
                    <ul class="nav nav-pills nav-stacked">
                        <?php foreach (Yii::$app->menu->getLevelContainer(2) as $child): ?>
                        <li <?php if ($child->isActive): ?>class="active" <?php endif; ?>><a href="<?= $child->link; ?>"><?= $child->title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-9">
                    <?= $content; ?>
                </div>
            <?php else: ?>
                <div>
                    <?= $content; ?>

                </div>
            <?php endif; ?>
</div>
<footer class="autoclear">
    <div class="wrap">
        <div class="row">
            <div class="fot-1 col-md-4">
                <div class="fwrap">
                    <h3 class="text-center">NUESTROS PRODUCTOS</h3>
                    <ul class="row">
                      <div  class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                        <li><a href="alquiler-de-equipos"> Alquiler de equipos</a></li>
                        <li><a href="oxigeno-medicinal"> Oxigeno medicinal</a></li>
                        <li><a href="oxigeno-industrial"> Oxigeno industrial</a></li>
                        <li><a href="accesorios"> Accesorios</a></li>
                      </div>
                      <div  class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                        <li><a href="equipos-ortopedicos"> Equipos ortopedicos</a></li>
                        <li><a href="novedades-y-ofertas"> Novedades y ofertas</a></li>
                        <li><a href="extintores"> Extintores</a></li>
                      </div>
                    </ul>
                </div>
            </div>
            <div class="fot-2 col-md-6">
                <div class="fwrap text-center">
                    <h3>DIRECCIÓN LA PAZ-BOLIVIA</h3>
                    <div id="datos">
                    <p>Calle Boquerón # 1318 casi esq. Almirante Grau, Z. San Pedro</p>
                    <p>Sucursal: Av. Diego de Peralta # 1274 frente al Hospital Holandes, Ciudad Satélite</p>
                    <i style="font-size: 1.2em" class="fa fa-phone"></i>  Telfs.: 2491279 – 2484992 <span class="opt"> &nbsp;&nbsp;&nbsp;</span> <i style="font-size: 1.2em" class="fa fa-mobile"></i>Cels.: 76522595 – 77776646</p>
                    <p><i style="font-size: .9em" class="fa fa-envelope"></i> oximedbolivia@hotmail.com  <a href="https://www.google.com/maps/d/embed?mid=1sFYDFI24iLZvycH971nEVYQOBRg" target="_blank">>>Ver mapa <i style="font-size: 1.1em" class="fa fa-map-marker"> </i></a></p>
                    </div>
                </div>
            </div>
            <div class="fot-3 col-md-2">
                <div class="fwrap text-center">
                    <h3>OXIMED BOLIVIA</h3>
                    <p>Visitenos en: </p>
                    <div class="botsocial">
                        <p>
                            <a href="#" onclick="void();" target="_blank"><img src="img/youtube.png" alt="próximamente en Youtube"></a>
                            <a href="#" target="_blank"><img src="img/google-plus.png" alt="próximamente en Google +"></a>
                            <a href="#" target="_blank"><img src="img/facebook.png" alt="próximamente en Facebook"></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class=" col-xs-12 col-sm-7 text-center">
              <p>Copyright &REG; OXIMED BOLIVIA &COPY; 2016 TODOS LOS DERECHOS RESERVADOS</p>
            </div>
            <div class=" col-xs-12 col-sm-5 text-center">
              <p>DISEÑO Y PROGRAMACIÓN <a href="//ahpublic.com" target="_blank">Ah! Publicidad</a> - <a href="//gnb.com.bo" target="_blank">GNB</a></p>
            </div>
       </div>
    </div>
    </footer>
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
