  $(function() {
     $('#nav > div').hover(
        function () {
            var $this = $(this);
            $this.find('img').stop().animate({
              'width'     :'192px',
              'height'    :'110px',
              'top'       :'-5px',
              'left'      :'-70px',
              'opacity'   :'1.0'
            },500,'easeOutBack',function(){
                $(this).parent().find('ul').fadeIn(700);
            });
            $this.find('a:first,h2').addClass('active');
        },
        function () {
            var $this = $(this);
            $this.find('ul').fadeOut(500);
            $this.find('img').stop().animate({
                'width'     :'52px',
                'height'    :'52px',
                'top'       :'0px',
                'left'      :'0px',
                'opacity'   :'0.1'
            },5000,'easeOutBack');
            $this.find('a:first,h2').removeClass('active');
        }
    );
    });

  $(document).ready(function(){
    //This is the code used on the exemple above
    $('.mySlideshow').edslider({
      width:100+'%',
      height: 360	//for more options check the options list below
    });
  });
