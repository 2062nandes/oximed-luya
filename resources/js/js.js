$(document).ready(function(){
  //.aslwrap
  function starterAnim(){
      $("#fakebg").addClass('loadedb');
      setTimeout(function(){ $("#fakebg").css("display", "none") }, 700);
      thedelay = 100;

      console.log('imageloadedagain'+thedelay);
      //logo animation
      setTimeout(function(){ console.log("adding classes"); $(".logowraporig").addClass("logowrap"); $(".aslwraporig").addClass("aslwrap")}, thedelay);
      thedelay+= 1600;

      //botones nav

      for (var i=1; i<=$(".colbtn").length; i++){
        $('.colbtn:nth-child('+i+')').delay(thedelay).animate({opacity: '1', left: '0'});
        thedelay+=100;
      }

      thedelay+= 300;
      //botones topnav
      for (var i=1; i<=$(".topnav .col33").length; i++){
        $('.topnav .col33:nth-child('+i+')').delay(thedelay).animate({opacity: '1', top: '0'});
        thedelay+=200;
      }
      //rubro

      $(".rubro").animate({opacity: '1'}, 500);
//      setTimeout(function(){ $(".ojowraporig").addClass("ojowrap")}, thedelay);
  //    thedelay+=2900;
      setTimeout(function(){ $(".rubro").addClass("rubroanim"); },thedelay);
  }

  /*verify the loading of images*/
  //list of images
var tover = ['bgheader.jpg', 'logo.png']; //just the name of a few images
  all = tover.length;
  var counter = 0;
  for ( var i=0; i < all; i++){
    var img = new Image();
    img.onload = function(){
      counter+=1;
      console.log($(this).attr('src')+' - cargada!');
      console.log("inner counter"+counter);
      if (counter == all){
        starterAnim();
        console.log("now is all charged");
      }
    }
    img.src="img/"+tover[i]; //here we define the path
  }
  console.log("all"+all);


  //$('img').each(function(){
  //    var img = new Image();
  //    img.onload = function() {
  //        console.log($(this).attr('src') + ' - done!');
  //    }
  //    img.src = $(this).attr('src');
  //});

  //var img = new Image();
  //var thedelay = 0;
  //img.onload = function() {
  //  starterAnim();
  //  console.log("you've reached here "+img.src);
  //}
  //img.src = "img/bg-1.jpg";

  function setTime(selector, clase, delay){
    setTimeout(function(){$(selector).addClass(clase)}, delay);
  }

/*
  $("body.vegas").vegas({
        timer:false,
        valign:'top',
        delay:7000,
        slides: [
            { src: "img/bg-1.jpg" },
            { src: "img/bg-2.jpg" },
            { src: "img/bg-4.jpg" },
            { src: "img/bg-5.jpg" },
            { src: "img/bg-3.jpg" }
        ],
        walk: function (index, slideSettings) {
          //texts
          var before = "<span class='slinner'>";
          var after = "</span>";
          if (index == 0){ $(".slogan").html(before+"Letreros luminosos"+after); }
          if (index == 1){ $(".slogan").html(before+"Gigantografías y banners"+after); }
          if (index == 2){ $(".slogan").html(before+"Rotulado de vehículos"+after); }
          if (index == 3){ $(".slogan").html(before+"Señalética y señalización"+after); }
          if (index == 4){ $(".slogan").html(before+"Artículos publicitarios"+after); }
          //the animation steps
          if (index == 0 || index == 1 || index == 2 || index == 3 || index == 4){
            $(".slogan").removeClass("leftentry");
            $(".slogan").removeClass("rightout");
            $(".slogan").addClass("leftentry");
            setTimeout(function(){$(".slogan").addClass("rightout")}, 6000);
          }
       }
  });
*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });

  var element = document.querySelector('.line');
  var panning = new Alekei(element, { fps: 30, speedX: 30 });
  panning.play();

  //lightbox
  //first to handle the case when
  $('.trig_wrap .fa').click(function(e){
    e.preventDefault();
    console.log("you're here");
    $(this).next().click();
  });
  $('.lightbox_trigger').click(function(e) {
    e.preventDefault();
    var image_href = $(this).attr("src");

    if ($('#lightbox').length > 0) { // #lightbox exists
      $('#content').html('<div class="imgwrap"><div class="prevbtn"></div><img class="mainimg" src="' + image_href + '" /><div class="closeimg"></div><div class="nextbtn"></div></div>');

      $('#lightbox').show("fast");
    }

    else { //#lightbox does not exist - create and insert (runs 1st time only)

      var lightbox =
      '<div id="lightbox">' +
        '<div id="content">' + //insert clicked link's href into img src
          '<div class="imgwrap">' +
            '<div class="prevbtn"></div>' +
            '<img class="mainimg" src="' + image_href +'" />' +
            '<div class="closeimg"></div>' +
            '<div class="nextbtn"></div>'
          '</div>' +
        '</div>' +
      '</div>';

      //insert lightbox HTML into page
      $('body').append(lightbox);
    }
  });

  var imgList = [];
  //makes a list of the elements with the class .lightbox_trigger below .lgroup li
  function makeList(){
    $(".lightbox_trigger").each(function(){ //es el disparador can be a class or another identifier
      href = $(this).attr("src");
      imgList.push(href);
    });
  }
  makeList();

  //Click on the button to close
  $(document).on('click','.closeimg', function() { //must use on in replacement of old live, as the lightbox element is inserted into the DOM
    $('#lightbox').hide("fast");
  });
  $(document).on('click', '.nextbtn', function() {
    src = $(".mainimg").attr("src");
    pos = jQuery.inArray(src, imgList);
    if ( (pos+1) == imgList.length){
      nextpos = 0;
    }
    else{
      nextpos = pos+1;
    }
    $(".mainimg").attr("src", imgList[nextpos]);
  });
  $(document).on('click', '.prevbtn', function() {
    src = $(".mainimg").attr("src");
    pos = jQuery.inArray(src, imgList);
    if ( pos == 0){
      prevpos = imgList.length-1;
    }
    else{
      prevpos = pos-1;
    }
    $(".mainimg").attr("src", imgList[prevpos]);
  });
});
