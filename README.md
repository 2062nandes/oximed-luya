<p align="center">
  <img src="https://luya.io/img/luya_logo_flat_icon.png" alt="LUYA Logo"/>
</p>
#WEB SITE FOR OXIMED BOLIVIA - SEMI-AUTOADMINISTRABLE 
-----------------------------------
**Theme Name:** Web Design & programing for Oximed Bolivia
**Theme URI:** [www.oximedbolivia.com.bo](http://www.oximedbolivia.com.bo/)

**Author:** Fernando Javier Averanga Aruquipa
**Author URI:** nandes.ingsistemas@gmail.com
**Gitlab:** [https://gitlab.com/2062nandes](https://gitlab.com/2062nandes)

![OXIMED DESIGN](https://gitlab.com/2062nandes/oximed-luya/raw/01842d60100fdc6a6735bd08081ed2d2dbe78c4e/public_html/img/screenshot.jpg)

[LUYA](https://gitter.im/luyadev/luya) is a [Yii 2 PHP Framework](https://github.com/yiisoft/yii2) wrapper which provides out of the box functions like an **administration interface**, a beautiful looking **content management system**, **payment** modules, **agency workflows** and other tools to develop your website pretty fast!
