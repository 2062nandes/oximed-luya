<?php
/*ImageBlock*/
public function twigFrontend()
{
    return '{% if extras.image is not empty %}
            <li>
                    {% if extras.link %}
                        <a class="text-teaser" href="#{{ extras.link }}"{% if extras.linkIsExternal %}{% endif %}>
                    {% endif %}
                          <img class="{{cfgs.cssClass}}" src="{{extras.image.source}}" {% if vars.caption is not empty %}alt="{{ extras.link }}" title="{{vars.caption}}"{% endif %}{% if cfgs.width %}{% endif %} />
                      <!--  <img class="{{cfgs.cssClass}}" src="{{extras.image.source}}" {% if vars.caption is not empty %}alt="{{vars.caption}}" title="{{vars.caption}}"{% endif %}{% if cfgs.width %} width="{{cfgs.width}}"{% endif %}{% if cfgs.height %} height="{{cfgs.height}}"{% endif %} /> -->
                          <span>Ver detalles <i class="fa fa-search-plus" aria-hidden="true"></i></span>
                    {% if extras.link %}
                        </a>
                        <div class="lb-overlay" id="{{ extras.link }}">
                          <img class="img-responsive center-block {{cfgs.cssClass}}" src="{{extras.image.source}}" {% if vars.caption is not empty %}alt="{{ extras.link }}" title="{{vars.caption}}"{% endif %}{% if cfgs.width %}{% endif %} />

                    {% endif %}

                    {% if extras.text is not empty %}

                        {% if vars.textType == 1 %}{{ extras.text }}{% else %}<p style="text-align:center; font-size:20px; color:rgb(14, 14, 14);">{{ extras.text|nl2br }}</p>{% endif %}
                        <a href="#page" class="lb-close"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Cerrar</a>
                    </div>
                    {% if vars.textType == 1 %}{{ extras.text }}{% else %}<p text-align:center; font-size:20px; color:rgb(14, 14, 14);>{{ extras.text|nl2br }}</p>{% endif %}
                    {% endif %}
          </li>
        {% endif %}';
}

 ?>
