-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-01-2017 a las 23:11:50
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `oximeddb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_auth`
--

CREATE TABLE `admin_auth` (
  `id` int(11) NOT NULL,
  `alias_name` varchar(60) NOT NULL,
  `module_name` varchar(60) NOT NULL,
  `is_crud` smallint(1) DEFAULT '0',
  `route` varchar(200) DEFAULT NULL,
  `api` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_auth`
--

INSERT INTO `admin_auth` (`id`, `alias_name`, `module_name`, `is_crud`, `route`, `api`) VALUES
(1, 'menu_access_item_user', 'admin', 1, '0', 'api-admin-user'),
(2, 'menu_access_item_group', 'admin', 1, '0', 'api-admin-group'),
(3, 'menu_system_item_language', 'admin', 1, '0', 'api-admin-lang'),
(4, 'menu_system_item_tags', 'admin', 1, '0', 'api-admin-tag'),
(5, 'menu_system_logger', 'admin', 1, '0', 'api-admin-logger'),
(6, 'menu_images_item_effects', 'admin', 1, '0', 'api-admin-effect'),
(7, 'menu_images_item_filters', 'admin', 1, '0', 'api-admin-filter'),
(8, 'menu_node_filemanager', 'admin', 0, 'admin/storage/index', '0'),
(9, 'Bloques página de contenido', 'cmsadmin', 1, '0', 'api-cms-navitempageblockitem'),
(10, 'menu_group_item_env_container', 'cmsadmin', 1, '0', 'api-cms-navcontainer'),
(11, 'menu_group_item_env_layouts', 'cmsadmin', 1, '0', 'api-cms-layout'),
(12, 'menu_group_item_elements_group', 'cmsadmin', 1, '0', 'api-cms-blockgroup'),
(13, 'menu_group_item_elements_blocks', 'cmsadmin', 1, '0', 'api-cms-block'),
(14, 'Crear nueva página', 'cmsadmin', 0, 'cmsadmin/page/create', '0'),
(15, 'Editar página', 'cmsadmin', 0, 'cmsadmin/page/update', '0'),
(16, 'Editar borrador', 'cmsadmin', 0, 'cmsadmin/page/drafts', '0'),
(17, 'menu_node_cms', 'cmsadmin', 0, 'cmsadmin/default/index', '0'),
(18, 'menu_group_item_env_permission', 'cmsadmin', 0, 'cmsadmin/permission/index', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_config`
--

CREATE TABLE `admin_config` (
  `name` varchar(80) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_config`
--

INSERT INTO `admin_config` (`name`, `value`) VALUES
('last_import_timestamp', '1483481048'),
('rc1_block_classes_renameing', '1'),
('setup_command_timestamp', '1480446410');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_group`
--

CREATE TABLE `admin_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_group`
--

INSERT INTO `admin_group` (`id`, `name`, `text`, `is_deleted`) VALUES
(1, 'Administrador', 'Administrador del sitio web', 0),
(2, 'Usuario', 'Administracion Personal de Oximed', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_group_auth`
--

CREATE TABLE `admin_group_auth` (
  `group_id` int(11) DEFAULT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `crud_create` smallint(4) DEFAULT NULL,
  `crud_update` smallint(4) DEFAULT NULL,
  `crud_delete` smallint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_group_auth`
--

INSERT INTO `admin_group_auth` (`group_id`, `auth_id`, `crud_create`, `crud_update`, `crud_delete`) VALUES
(1, 1, 1, 1, 1),
(1, 2, 1, 1, 1),
(1, 3, 1, 1, 1),
(1, 4, 1, 1, 1),
(1, 5, 1, 1, 1),
(1, 6, 1, 1, 1),
(1, 7, 1, 1, 1),
(1, 8, 1, 1, 1),
(1, 9, 1, 1, 1),
(1, 10, 1, 1, 1),
(1, 11, 1, 1, 1),
(1, 12, 1, 1, 1),
(1, 13, 1, 1, 1),
(1, 14, 1, 1, 1),
(1, 15, 1, 1, 1),
(1, 16, 1, 1, 1),
(1, 17, 1, 1, 1),
(1, 18, 1, 1, 1),
(2, 8, 0, 0, 0),
(2, 9, 1, 1, 1),
(2, 15, 0, 0, 0),
(2, 17, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_lang`
--

CREATE TABLE `admin_lang` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `short_code` varchar(255) DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_lang`
--

INSERT INTO `admin_lang` (`id`, `name`, `short_code`, `is_default`) VALUES
(1, 'English', 'en', 0),
(2, 'Español', 'es', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_logger`
--

CREATE TABLE `admin_logger` (
  `id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `message` text NOT NULL,
  `type` int(11) NOT NULL,
  `trace_file` varchar(255) DEFAULT NULL,
  `trace_line` varchar(255) DEFAULT NULL,
  `trace_function` varchar(255) DEFAULT NULL,
  `trace_function_args` text,
  `group_identifier` varchar(255) DEFAULT NULL,
  `group_identifier_index` int(11) DEFAULT NULL,
  `get` text,
  `post` text,
  `session` text,
  `server` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_ngrest_log`
--

CREATE TABLE `admin_ngrest_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_create` int(11) NOT NULL,
  `route` varchar(80) NOT NULL,
  `api` varchar(80) NOT NULL,
  `is_update` tinyint(1) DEFAULT '0',
  `is_insert` tinyint(1) DEFAULT '0',
  `attributes_json` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_ngrest_log`
--

INSERT INTO `admin_ngrest_log` (`id`, `user_id`, `timestamp_create`, `route`, `api`, `is_update`, `is_insert`, `attributes_json`) VALUES
(1, 1, 1480446514, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":null}'),
(2, 1, 1480446532, '', 'api-admin-lang', 0, 1, '{"id":2,"name":"Espa\\u00f1ol","short_code":"es","is_default":0}'),
(3, 1, 1480446798, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":null}'),
(4, 1, 1480446882, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(5, 1, 1480447084, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(6, 1, 1480447131, '', 'api-admin-lang', 1, 0, '{"id":2,"name":"Espa\\u00f1ol","short_code":"es","is_default":1}'),
(7, 1, 1480447143, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(8, 1, 1480447194, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(9, 1, 1480447373, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(10, 1, 1480447419, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(11, 1, 1480447627, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(12, 1, 1480447812, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(13, 1, 1480447857, '', 'api-cms-navcontainer', 1, 0, '{"id":1,"name":"Inicio","alias":"inicio","is_deleted":0}'),
(14, 1, 1480447860, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(15, 1, 1480447912, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(16, 1, 1480448083, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(17, 1, 1480539985, '', 'api-admin-user', 0, 1, '{"id":2,"firstname":"Oximed","lastname":"Oxigeno medicinal","title":0,"email":"oximedbolivia@hotmail.com","password":"$2y$13$E9RyiB5yuIWNvjBNNytSeOnQ\\/ourm6kavL\\/wkbeNTh0N6YrUo34yS","password_salt":"dEZLckuLwYnCcQ33qQhgB4PuHIPBAhNX","auth_token":"","is_deleted":0,"secure_token":null,"secure_token_timestamp":null,"force_reload":null,"settings":null}'),
(18, 1, 1480540160, '', 'api-admin-group', 0, 1, '{"id":2,"name":"Usuario","text":"Administracion Personal de Oximed","is_deleted":null}'),
(19, 1, 1480540199, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"2bab48d8ce5b02fdb9f9e78e70ab9bfa81b2096ccf8a0c0308fbf517c5a545476hfVfH8VFpuLY37fe-4AA5WJcGyztz-N","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1}}"}'),
(20, 1, 1480540397, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"2bab48d8ce5b02fdb9f9e78e70ab9bfa81b2096ccf8a0c0308fbf517c5a545476hfVfH8VFpuLY37fe-4AA5WJcGyztz-N","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1}}"}'),
(21, 2, 1480540402, '', 'api-admin-user', 1, 0, '{"id":2,"firstname":"Oximed","lastname":"Oxigeno medicinal","title":0,"email":"oximedbolivia@hotmail.com","password":"$2y$13$E9RyiB5yuIWNvjBNNytSeOnQ\\/ourm6kavL\\/wkbeNTh0N6YrUo34yS","password_salt":"dEZLckuLwYnCcQ33qQhgB4PuHIPBAhNX","auth_token":"abd0d5b9da5e1bc6fc184bae96b1a1134b5270bd8669a0db40f2e92eb38631c0SOUeZpYrSje3QLrpUj1Doc20UK1FPc5H","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":null}'),
(22, 2, 1480540462, '', 'api-admin-user', 1, 0, '{"id":2,"firstname":"Oximed","lastname":"Oxigeno medicinal","title":0,"email":"oximedbolivia@hotmail.com","password":"$2y$13$E9RyiB5yuIWNvjBNNytSeOnQ\\/ourm6kavL\\/wkbeNTh0N6YrUo34yS","password_salt":"dEZLckuLwYnCcQ33qQhgB4PuHIPBAhNX","auth_token":"abd0d5b9da5e1bc6fc184bae96b1a1134b5270bd8669a0db40f2e92eb38631c0SOUeZpYrSje3QLrpUj1Doc20UK1FPc5H","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":null}'),
(23, 1, 1480597310, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"6833df96b73e70d6d9635ab09edf93869791baa56be1b6ea41fe4054b07bec42ICXbOAd82vgKVoLx_Vzn34K_aFjE_8k8","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(24, 1, 1480597526, '', 'api-admin-user', 1, 0, '{"id":2,"firstname":"Oximed","lastname":"Bolivia","title":0,"email":"oximedbolivia@hotmail.com","password":"$2y$13$E9RyiB5yuIWNvjBNNytSeOnQ\\/ourm6kavL\\/wkbeNTh0N6YrUo34yS","password_salt":"dEZLckuLwYnCcQ33qQhgB4PuHIPBAhNX","auth_token":"de3055b34ddf849a7cafc4affa7721bb71794ebd52359efafa0e717af22521e0J1N_d2togqQGl-wKYy-x9XNw_sSe5XVk","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(25, 1, 1480597585, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"6833df96b73e70d6d9635ab09edf93869791baa56be1b6ea41fe4054b07bec42ICXbOAd82vgKVoLx_Vzn34K_aFjE_8k8","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(26, 1, 1480597914, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"6833df96b73e70d6d9635ab09edf93869791baa56be1b6ea41fe4054b07bec42ICXbOAd82vgKVoLx_Vzn34K_aFjE_8k8","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(27, 1, 1480597998, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"6833df96b73e70d6d9635ab09edf93869791baa56be1b6ea41fe4054b07bec42ICXbOAd82vgKVoLx_Vzn34K_aFjE_8k8","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(28, 1, 1480629878, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando","lastname":"Averanga","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"d68c329117df10f74aa50595655fe3eaf34cbf479f47e6af529d4d308901b3e78gn0BZf2KckMFTJnM5nPTswk6miK71pR","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(29, 1, 1480629935, '', 'api-admin-group', 1, 0, '{"id":1,"name":"Administrador","text":"Administrador del sitio web","is_deleted":0}'),
(30, 1, 1480629961, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando Javier","lastname":"Averanga Aruquipa","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"d68c329117df10f74aa50595655fe3eaf34cbf479f47e6af529d4d308901b3e78gn0BZf2KckMFTJnM5nPTswk6miK71pR","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(31, 1, 1480631172, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando Javier","lastname":"Averanga Aruquipa","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"d68c329117df10f74aa50595655fe3eaf34cbf479f47e6af529d4d308901b3e78gn0BZf2KckMFTJnM5nPTswk6miK71pR","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(32, 1, 1480710109, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando Javier","lastname":"Averanga Aruquipa","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"304c2c334fae5553c6ab8b71a38ab5bb7357a2f2c88b9e824ff732a76749b9ebLyw1jey9q3lp8AD0rXi81lUdUbODLE9y","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(33, 2, 1480714793, '', 'api-admin-user', 1, 0, '{"id":2,"firstname":"Oximed","lastname":"Bolivia","title":0,"email":"oximedbolivia@hotmail.com","password":"$2y$13$E9RyiB5yuIWNvjBNNytSeOnQ\\/ourm6kavL\\/wkbeNTh0N6YrUo34yS","password_salt":"dEZLckuLwYnCcQ33qQhgB4PuHIPBAhNX","auth_token":"c82f41851e4e4d400ccaf69792e0e72d178703946b9edcb94b5730db2801dfd94tgBKrS4wgq8_hK5fXdYZtBcN3uL8Hpb","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0}}"}'),
(34, 1, 1481294942, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando Javier","lastname":"Averanga Aruquipa","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"c052e9c6e8f8a82a43c69bbcb61bbfb28fbb4b5e6add3582b8c3bdec97a805b4Bqbs_LbD93-nuvmuNHN_zrYEVtYnSECN","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(35, 1, 1483474888, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando Javier","lastname":"Averanga Aruquipa","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"b02dca1fdf5cc92ad6af452b38df6fd1067674401ce6e972efa5491cf59c1827PW6an_-RNOV5ValrrkOv8GzBvUe63mpM","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(36, 1, 1483476730, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando Javier","lastname":"Averanga Aruquipa","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"7b29e140e936f9f6a99ce959525bf30a7a4a5eba30353edd917185309cb2e976vkYsm5JrlaLjyf5KU_1Hl-m8nMDyROvV","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}'),
(37, 1, 1483481064, '', 'api-admin-user', 1, 0, '{"id":1,"firstname":"Fernando Javier","lastname":"Averanga Aruquipa","title":1,"email":"nandes.ingsistemas@gmail.com","password":"$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK\\/GTMluqszhpGDYR0H7Sje","password_salt":"j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_","auth_token":"7b29e140e936f9f6a99ce959525bf30a7a4a5eba30353edd917185309cb2e976vkYsm5JrlaLjyf5KU_1Hl-m8nMDyROvV","is_deleted":0,"secure_token":null,"secure_token_timestamp":0,"force_reload":0,"settings":"{\\"togglecat\\":{\\"1\\":0},\\"togglegroup\\":{\\"2\\":1,\\"4\\":1,\\"3\\":1,\\"6\\":1,\\"1\\":1}}"}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_property`
--

CREATE TABLE `admin_property` (
  `id` int(11) NOT NULL,
  `module_name` varchar(120) DEFAULT NULL,
  `var_name` varchar(80) NOT NULL,
  `class_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_search_data`
--

CREATE TABLE `admin_search_data` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_create` int(11) NOT NULL,
  `query` varchar(200) NOT NULL,
  `num_rows` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_storage_effect`
--

CREATE TABLE `admin_storage_effect` (
  `id` int(11) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `imagine_name` varchar(255) DEFAULT NULL,
  `imagine_json_params` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_storage_effect`
--

INSERT INTO `admin_storage_effect` (`id`, `identifier`, `name`, `imagine_name`, `imagine_json_params`) VALUES
(1, 'thumbnail', 'Thumbnail', 'thumbnail', '{"vars":[{"var":"width","label":"Breit in Pixel"},{"var":"height","label":"Hoehe in Pixel"},{"var":"mode","label":"outbound or inset"},{"var":"saveOptions","label":"save options"}]}'),
(2, 'crop', 'Crop', 'crop', '{"vars":[{"var":"width","label":"Breit in Pixel"},{"var":"height","label":"Hoehe in Pixel"},{"var":"saveOptions","label":"save options"}]}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_storage_file`
--

CREATE TABLE `admin_storage_file` (
  `id` int(11) NOT NULL,
  `is_hidden` tinyint(1) DEFAULT '0',
  `folder_id` int(11) DEFAULT '0',
  `name_original` varchar(255) DEFAULT NULL,
  `name_new` varchar(255) DEFAULT NULL,
  `name_new_compound` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `hash_file` varchar(255) DEFAULT NULL,
  `hash_name` varchar(255) DEFAULT NULL,
  `upload_timestamp` int(11) NOT NULL DEFAULT '0',
  `file_size` int(11) DEFAULT '0',
  `upload_user_id` int(11) DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `passthrough_file` tinyint(1) DEFAULT '0',
  `passthrough_file_password` varchar(40) DEFAULT NULL,
  `passthrough_file_stats` int(11) DEFAULT '0',
  `caption` text,
  `internal_note` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_storage_file`
--

INSERT INTO `admin_storage_file` (`id`, `is_hidden`, `folder_id`, `name_original`, `name_new`, `name_new_compound`, `mime_type`, `extension`, `hash_file`, `hash_name`, `upload_timestamp`, `file_size`, `upload_user_id`, `is_deleted`, `passthrough_file`, `passthrough_file_password`, `passthrough_file_stats`, `caption`, `internal_note`) VALUES
(1, 0, 0, '734156_362780960520314_544233595_n.jpg', '734156362780960520314544233595n', '734156362780960520314544233595n_3cc74651.jpg', 'image/jpeg', 'jpg', 'd9ac9261ca2d6166fc28cc5b7a84fbdc', '3cc74651', 1480537039, 61792, 1, 1, 0, NULL, 0, NULL, NULL),
(2, 0, 0, 'imagen-de-prueba.jpg', 'imagen-de-prueba', 'imagen-de-prueba_4c16f205.jpg', 'image/jpeg', 'jpg', 'dbb07edc65c3890f839eda636d8bcd3e', '4c16f205', 1480630006, 124097, 1, 1, 0, NULL, 0, NULL, NULL),
(3, 0, 0, 'pBeeJQDQ.png', 'pbeejqdq', 'pbeejqdq_7ffb59f9.png', 'image/png', 'png', '7b355fcce47c1def8d8d6adf11d97741', '7ffb59f9', 1480637583, 42497, 1, 1, 0, NULL, 0, NULL, NULL),
(4, 0, 0, 'imagen-de-prueba.jpg', 'imagen-de-prueba', 'imagen-de-prueba_d59f793b.jpg', 'image/jpeg', 'jpg', 'dbb07edc65c3890f839eda636d8bcd3e', 'd59f793b', 1483476345, 124097, 1, 0, 0, NULL, 0, NULL, NULL),
(5, 0, 0, 'alqui-5.jpg', 'alqui-5', 'alqui-5_e11e92d6.jpg', 'image/jpeg', 'jpg', 'cc8a0a617e5f65b3f1414f2dea40f443', 'e11e92d6', 1483480561, 14735, 1, 0, 0, NULL, 0, NULL, NULL),
(6, 0, 0, 'acce-6.jpg', 'acce-6', 'acce-6_919be13a.jpg', 'image/jpeg', 'jpg', 'deb42989380b7ce67d264c2ce9a0aeae', '919be13a', 1483481155, 8438, 1, 0, 0, NULL, 0, NULL, NULL),
(7, 0, 0, 'ortopedico-16.jpg', 'ortopedico-16', 'ortopedico-16_7524bc5c.jpg', 'image/jpeg', 'jpg', '56b2835e47f6746872678138abd7f0ed', '7524bc5c', 1483481199, 9434, 1, 0, 0, NULL, 0, NULL, NULL),
(8, 0, 0, 'alqui-2.jpg', 'alqui-2', 'alqui-2_595a1c11.jpg', 'image/jpeg', 'jpg', '6b607963c5a8a110bef0df61e4501e80', '595a1c11', 1483481285, 13731, 1, 0, 0, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_storage_filter`
--

CREATE TABLE `admin_storage_filter` (
  `id` int(11) NOT NULL,
  `identifier` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_storage_filter`
--

INSERT INTO `admin_storage_filter` (`id`, `identifier`, `name`) VALUES
(1, 'large-crop', 'Crop large (800x800)'),
(2, 'large-thumbnail', 'Thumbnail large (800xnull)'),
(3, 'medium-crop', 'Crop medium (300x300)'),
(4, 'medium-thumbnail', 'Thumbnail medium (300xnull)'),
(5, 'small-crop', 'Crop small (100x100)'),
(6, 'small-landscape', 'Landscape small (150x50)'),
(7, 'small-thumbnail', 'Thumbnail small (100xnull)'),
(8, 'tiny-crop', 'Crop tiny (40x40)'),
(9, 'tiny-thumbnail', 'Thumbnail tiny (40xnull)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_storage_filter_chain`
--

CREATE TABLE `admin_storage_filter_chain` (
  `id` int(11) NOT NULL,
  `sort_index` int(11) DEFAULT NULL,
  `filter_id` int(11) DEFAULT NULL,
  `effect_id` int(11) DEFAULT NULL,
  `effect_json_values` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_storage_filter_chain`
--

INSERT INTO `admin_storage_filter_chain` (`id`, `sort_index`, `filter_id`, `effect_id`, `effect_json_values`) VALUES
(1, NULL, 1, 1, '{"width":800,"height":800}'),
(2, NULL, 2, 1, '{"width":800,"height":null}'),
(3, NULL, 3, 1, '{"width":300,"height":300}'),
(4, NULL, 4, 1, '{"width":300,"height":null}'),
(5, NULL, 5, 1, '{"width":100,"height":100}'),
(6, NULL, 6, 1, '{"width":150,"height":50}'),
(7, NULL, 7, 1, '{"width":100,"height":null}'),
(8, NULL, 8, 1, '{"width":40,"height":40}'),
(9, NULL, 9, 1, '{"width":40,"height":null}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_storage_folder`
--

CREATE TABLE `admin_storage_folder` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `timestamp_create` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_storage_image`
--

CREATE TABLE `admin_storage_image` (
  `id` int(11) NOT NULL,
  `file_id` int(11) DEFAULT NULL,
  `filter_id` int(11) DEFAULT NULL,
  `resolution_width` int(11) DEFAULT NULL,
  `resolution_height` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_storage_image`
--

INSERT INTO `admin_storage_image` (`id`, `file_id`, `filter_id`, `resolution_width`, `resolution_height`) VALUES
(1, 1, 8, 40, 40),
(2, 1, 4, 300, 227),
(3, 1, 0, 726, 547),
(4, 1, 1, 726, 547),
(5, 2, 8, 40, 40),
(6, 2, 4, 300, 300),
(7, 2, 0, 500, 500),
(8, 3, 8, 40, 40),
(9, 3, 4, 300, 300),
(10, 3, 0, 460, 460),
(11, 4, 8, 40, 40),
(12, 4, 4, 300, 300),
(13, 4, 0, 500, 500),
(14, 5, 8, 40, 40),
(15, 5, 4, 232, 232),
(16, 5, 0, 232, 232),
(17, 6, 8, 40, 40),
(18, 6, 4, 232, 232),
(19, 6, 0, 232, 232),
(20, 7, 8, 40, 40),
(21, 7, 4, 232, 232),
(22, 7, 0, 232, 232),
(23, 8, 8, 40, 40),
(24, 8, 4, 232, 232),
(25, 8, 0, 232, 232);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_tag`
--

CREATE TABLE `admin_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_tag_relation`
--

CREATE TABLE `admin_tag_relation` (
  `tag_id` int(11) NOT NULL,
  `table_name` varchar(120) NOT NULL,
  `pk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_user`
--

CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `title` smallint(6) DEFAULT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `password_salt` varchar(255) DEFAULT NULL,
  `auth_token` varchar(255) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  `secure_token` varchar(40) DEFAULT NULL,
  `secure_token_timestamp` int(11) DEFAULT '0',
  `force_reload` tinyint(1) DEFAULT '0',
  `settings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_user`
--

INSERT INTO `admin_user` (`id`, `firstname`, `lastname`, `title`, `email`, `password`, `password_salt`, `auth_token`, `is_deleted`, `secure_token`, `secure_token_timestamp`, `force_reload`, `settings`) VALUES
(1, 'Fernando Javier', 'Averanga Aruquipa', 1, 'nandes.ingsistemas@gmail.com', '$2y$13$V1Kqlb29aWBqztjlZQ6LvOOyZBKC0sQK/GTMluqszhpGDYR0H7Sje', 'j_v2xZtDHJjBizMsq6vpr3ox1ZYglc3_', '7b29e140e936f9f6a99ce959525bf30a7a4a5eba30353edd917185309cb2e976vkYsm5JrlaLjyf5KU_1Hl-m8nMDyROvV', 0, NULL, 0, 0, '{"togglecat":{"1":0},"togglegroup":{"2":1,"4":1,"3":1,"6":1,"1":1}}'),
(2, 'Oximed', 'Bolivia', 0, 'oximedbolivia@hotmail.com', '$2y$13$E9RyiB5yuIWNvjBNNytSeOnQ/ourm6kavL/wkbeNTh0N6YrUo34yS', 'dEZLckuLwYnCcQ33qQhgB4PuHIPBAhNX', '50d5f201581cd7c1844e0842c5dd2439bb386794a80a2014d731af7fdab5da17Myuyvpbj4JZ4A8sYY3uxJx8URZ5e3TXT', 0, NULL, 0, 1, '{"togglecat":{"1":0}}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_user_group`
--

CREATE TABLE `admin_user_group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_user_group`
--

INSERT INTO `admin_user_group` (`id`, `user_id`, `group_id`) VALUES
(2, 2, 2),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_user_login`
--

CREATE TABLE `admin_user_login` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp_create` int(11) NOT NULL,
  `auth_token` varchar(120) NOT NULL,
  `ip` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_user_login`
--

INSERT INTO `admin_user_login` (`id`, `user_id`, `timestamp_create`, `auth_token`, `ip`) VALUES
(1, 1, 1480446479, 'a05121ded39dd8633b4d58a4d47fefd2fbfff1e03435f7c29932a5b3fe42fea7seH7QFZ3jFq7PMKq5T3luORoF2Rby7ey', '::1'),
(2, 1, 1480448820, '26991a2268c0a33cc60ed940504750042d120139092421c780e8004cd3bbdc47jrWU0R-cVw4CnqSE0G-LZYoznedPRGIb', '::1'),
(3, 1, 1480450786, '8e6e450a4c159be2f6e8528f912523014cab2f96b77140a65169f2d3544952a1GUJmLv6tc-q3ADiuyrSywWx9mj-9uFVv', '::1'),
(4, 1, 1480510637, '451acc8b6bc01e17503e391890936b87a082a360fa2d195b811b6c7c6110595bFYDTb3JYC56t2I-qhVQIsnvYA-1onJ_9', '::1'),
(5, 2, 1480540056, 'cf5de357249045b1f5c82651c1e0dccd957cc976900d804e8b60b8bef118eff6iDzRztCBUOkN0bz9Y3Ni3ozsaHzqritE', '::1'),
(6, 1, 1480540106, '2bab48d8ce5b02fdb9f9e78e70ab9bfa81b2096ccf8a0c0308fbf517c5a545476hfVfH8VFpuLY37fe-4AA5WJcGyztz-N', '::1'),
(7, 2, 1480540349, 'abd0d5b9da5e1bc6fc184bae96b1a1134b5270bd8669a0db40f2e92eb38631c0SOUeZpYrSje3QLrpUj1Doc20UK1FPc5H', '::1'),
(8, 1, 1480542504, '55070e79304409e271bf77fc01302273b6db5e8c7e7c5207c8b3dd15460566ce15gPYgvChXTeC0ThnQXcdssNP4HtIoo1', '200.105.162.97'),
(9, 1, 1480547633, '5afc10cac03ca707946ca60c541dec28c25f4208ca5503b508eada3667bca2balkCxdMdVOaOULr931Kq2reWKmH7vi-_j', '186.2.32.158'),
(10, 1, 1480557175, 'acec0be4a6d4ee48c59d0ff611bfb0e77b40e57537366d902a4ce447dec6321eV7oLwowNXgv58vWfOkAu1bmKy-ioUiMj', '186.2.32.158'),
(11, 2, 1480562900, 'a0de20f1c31152c68ed62ce6e3933283ee6f093463f23bd266101180ef17e9736XoLKWahQERotXD2EdHELqxvnsfnLJ3O', '186.2.32.158'),
(12, 2, 1480591854, 'c5456410f66aaf62b08aa39ac7d56ecf466f4cc44a78a0de9e777dfe4b5e12c9u7z338DhJcQsyNGHeVupYrgn--f969Ut', '186.2.32.158'),
(13, 1, 1480596566, '6833df96b73e70d6d9635ab09edf93869791baa56be1b6ea41fe4054b07bec42ICXbOAd82vgKVoLx_Vzn34K_aFjE_8k8', '200.105.162.97'),
(14, 2, 1480597169, 'de3055b34ddf849a7cafc4affa7721bb71794ebd52359efafa0e717af22521e0J1N_d2togqQGl-wKYy-x9XNw_sSe5XVk', '200.105.162.97'),
(15, 1, 1480599248, '2f0f7db0969e7ddc026e1d93d38ae9b5a411e9b18d50658bdff8f5d8251ae054nvTaER0onLY7EKSVGLHd6gZSf4mbXiv3', '200.105.162.97'),
(16, 2, 1480607434, '38de71474e7fd8949197c3ec4d6a79b69c488e992cd547f575a9eea81cd1772dUAXcIz25UBYnQP0Gzkf_KBQDTPjBP5Y-', '200.105.162.97'),
(17, 1, 1480610581, 'd89770a7f6725405e81a4b94a2e08fd1cd38505927a3d3cc315951c596a25074GAf_-UTt4zKAjpFqrznly3GN8MkQYVF0', '200.105.162.97'),
(18, 1, 1480629807, 'd68c329117df10f74aa50595655fe3eaf34cbf479f47e6af529d4d308901b3e78gn0BZf2KckMFTJnM5nPTswk6miK71pR', '186.121.219.237'),
(19, 1, 1480637503, '21f2883e338890e3cd24d7d2ceadee978db8a4a8bad65aae05ef146de207a2e5tcfM5niKxngF9ciR7rA6TATGpIJFlwjY', '186.2.32.158'),
(20, 1, 1480681886, 'cbc5497391bdd2952c727542ca125e3d18f4adb3ff544259f8923cb1b8ef839c-Rj7A8juYcFBQ1vGEi2ShWBE5yORwraw', '186.121.219.237'),
(21, 1, 1480709682, '304c2c334fae5553c6ab8b71a38ab5bb7357a2f2c88b9e824ff732a76749b9ebLyw1jey9q3lp8AD0rXi81lUdUbODLE9y', '186.121.219.237'),
(22, 2, 1480714516, 'c82f41851e4e4d400ccaf69792e0e72d178703946b9edcb94b5730db2801dfd94tgBKrS4wgq8_hK5fXdYZtBcN3uL8Hpb', '186.121.219.237'),
(23, 2, 1480731547, '234cd53a750dd9152872602aed8642eabe1179f1ad14b62d7a08a1404bcf3103pQMNwODsXyuxOC8QcgZGVBf35EqYUS4Y', '186.2.32.158'),
(24, 2, 1480731550, '50d5f201581cd7c1844e0842c5dd2439bb386794a80a2014d731af7fdab5da17Myuyvpbj4JZ4A8sYY3uxJx8URZ5e3TXT', '186.2.32.158'),
(25, 1, 1480823096, '96e4f834b64b5af604f01e9cc20380db021b20a7d0b3372b1f7afcd484943266KDP9p6msqvoo6XnXstMWOVt5pZdByLNg', '186.2.32.158'),
(26, 1, 1480994917, '908f1ff8335d49470b358defa73295166169d130a9e042b7ea545db629088943vYP7RD6SxSBw0LF92N9PkwxwXKlzB0B6', '186.2.32.158'),
(27, 1, 1481294360, 'c052e9c6e8f8a82a43c69bbcb61bbfb28fbb4b5e6add3582b8c3bdec97a805b4Bqbs_LbD93-nuvmuNHN_zrYEVtYnSECN', '186.121.230.94'),
(28, 1, 1481322481, '227c8c829014d93c1b57229ad4c94926c595e566517000a58c113ad000e75940g4ATZqYQyM0VaWAc8JdzrZOC53UDvK1Q', '186.121.230.94'),
(29, 1, 1481389631, '5c884a0956d57cc0b71380accae7e4f8a9c05075b6bf72a567bbe695b846f7bc7vh37OGJYisMPp3PfnU9x0JMt1wTnQ90', '186.121.230.94'),
(30, 1, 1481505241, '3ace46cce3d568d07bf54e42894187bef768c1f5d10629db9bbd7f54caa35f094pA5gBV33ZT2S6TckJgj4mGfa2yb9G3m', '186.2.32.158'),
(31, 1, 1481506471, 'f9138de8203e96f57c5fe5d2844508db334b5c40e93660fc034bc244af0fb6b8ZKxm8KAM8bVFxBB60K6BJMNlnEMKuTQi', '186.2.32.158'),
(32, 1, 1483471270, 'b02dca1fdf5cc92ad6af452b38df6fd1067674401ce6e972efa5491cf59c1827PW6an_-RNOV5ValrrkOv8GzBvUe63mpM', '186.121.230.163'),
(33, 1, 1483476055, '7b29e140e936f9f6a99ce959525bf30a7a4a5eba30353edd917185309cb2e976vkYsm5JrlaLjyf5KU_1Hl-m8nMDyROvV', '::1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_user_online`
--

CREATE TABLE `admin_user_online` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_timestamp` int(11) NOT NULL,
  `invoken_route` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_block`
--

CREATE TABLE `cms_block` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_block`
--

INSERT INTO `cms_block` (`id`, `group_id`, `class`) VALUES
(1, 4, '\\luya\\cms\\frontend\\blocks\\AudioBlock'),
(2, 1, '\\luya\\cms\\frontend\\blocks\\DevBlock'),
(3, 3, '\\luya\\cms\\frontend\\blocks\\FileListBlock'),
(4, 3, '\\luya\\cms\\frontend\\blocks\\FormBlock'),
(5, 1, '\\luya\\cms\\frontend\\blocks\\HtmlBlock'),
(6, 4, '\\luya\\cms\\frontend\\blocks\\ImageBlock'),
(7, 4, '\\luya\\cms\\frontend\\blocks\\ImageTextBlock'),
(8, 2, '\\luya\\cms\\frontend\\blocks\\LayoutBlock'),
(9, 3, '\\luya\\cms\\frontend\\blocks\\LineBlock'),
(10, 3, '\\luya\\cms\\frontend\\blocks\\LinkButtonBlock'),
(11, 6, '\\luya\\cms\\frontend\\blocks\\ListBlock'),
(12, 3, '\\luya\\cms\\frontend\\blocks\\MapBlock'),
(13, 1, '\\luya\\cms\\frontend\\blocks\\ModuleBlock'),
(14, 6, '\\luya\\cms\\frontend\\blocks\\QuoteBlock'),
(15, 3, '\\luya\\cms\\frontend\\blocks\\SpacingBlock'),
(16, 3, '\\luya\\cms\\frontend\\blocks\\TableBlock'),
(17, 6, '\\luya\\cms\\frontend\\blocks\\TextBlock'),
(18, 6, '\\luya\\cms\\frontend\\blocks\\TitleBlock'),
(19, 4, '\\luya\\cms\\frontend\\blocks\\VideoBlock'),
(20, 6, '\\luya\\cms\\frontend\\blocks\\WysiwygBlock');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_block_group`
--

CREATE TABLE `cms_block_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `identifier` varchar(120) NOT NULL,
  `created_timestamp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_block_group`
--

INSERT INTO `cms_block_group` (`id`, `name`, `is_deleted`, `identifier`, `created_timestamp`) VALUES
(1, 'Desarrollo', 0, 'development-group', 1480446330),
(2, 'Diseño', 0, 'layout-group', 1480446331),
(3, 'Lo esencial', 0, 'main-group', 1480446331),
(4, 'Medio', 0, 'media-group', 1480446331),
(5, 'Proyecto', 0, 'project-group', 1480446331),
(6, 'Textos', 0, 'text-group', 1480446331);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_layout`
--

CREATE TABLE `cms_layout` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `json_config` text,
  `view_file` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_layout`
--

INSERT INTO `cms_layout` (`id`, `name`, `json_config`, `view_file`) VALUES
(1, 'Main', '{"placeholders":[{"label":"Content","var":"content"}]}', 'main.php'),
(2, 'Novedades Y Ofertas', '{"placeholders":[{"label":"Titulo","var":"TITULO"},{"label":"Insertarimagen","var":"insertarimagen"}]}', 'novedades-y-ofertas.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_log`
--

CREATE TABLE `cms_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `is_insertion` tinyint(1) DEFAULT '0',
  `is_update` tinyint(1) DEFAULT '0',
  `is_deletion` tinyint(1) DEFAULT '0',
  `timestamp` int(11) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `data_json` text,
  `table_name` varchar(120) DEFAULT NULL,
  `row_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_log`
--

INSERT INTO `cms_log` (`id`, `user_id`, `is_insertion`, `is_update`, `is_deletion`, `timestamp`, `message`, `data_json`, `table_name`, `row_id`) VALUES
(1, 1, 1, 0, 0, 1480446838, '{"tableName":"cms_nav_item","action":"insert","row":2}', '{"nav_id":1,"lang_id":"2","nav_item_type":1,"nav_item_type_id":1,"title":"Homepage","alias":"homepage","description":null,"keywords":null,"title_tag":null,"timestamp_create":1480446838,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":2}', 'cms_nav_item', 2),
(2, 1, 0, 1, 0, 1480446839, '{"tableName":"cms_nav_item","action":"update","row":2}', '{"nav_id":1,"lang_id":"2","nav_item_type":1,"nav_item_type_id":2,"title":"Homepage","alias":"homepage","description":null,"keywords":null,"title_tag":null,"timestamp_create":1480446838,"timestamp_update":1480446838,"create_user_id":1,"update_user_id":1,"id":2}', 'cms_nav_item', 2),
(3, 1, 0, 1, 0, 1480446870, '{"tableName":"cms_nav_item","action":"update","row":1}', '{"id":1,"nav_id":1,"lang_id":1,"nav_item_type":1,"nav_item_type_id":1,"create_user_id":1,"update_user_id":1,"timestamp_create":1480446410,"timestamp_update":1480446869,"title":"Homepage","alias":"2016-11-29-20-14-homepage","description":null,"keywords":null,"title_tag":null}', 'cms_nav_item', 1),
(4, 1, 0, 1, 0, 1480446870, '{"tableName":"cms_nav_item","action":"update","row":2}', '{"id":2,"nav_id":1,"lang_id":2,"nav_item_type":1,"nav_item_type_id":2,"create_user_id":1,"update_user_id":1,"timestamp_create":1480446838,"timestamp_update":1480446870,"title":"Homepage","alias":"2016-11-29-20-14-homepage","description":null,"keywords":null,"title_tag":null}', 'cms_nav_item', 2),
(5, 1, 0, 1, 0, 1480446870, '{"tableName":"cms_nav","action":"update","row":1}', '{"id":1,"nav_container_id":1,"parent_nav_id":0,"sort_index":0,"is_deleted":1,"is_hidden":0,"is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 1),
(6, 1, 1, 0, 0, 1480446924, '{"tableName":"cms_nav","action":"insert","row":2}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":2,"id":2}', 'cms_nav', 2),
(7, 1, 1, 0, 0, 1480446925, '{"tableName":"cms_nav_item","action":"insert","row":3}', '{"lang_id":"1","title":"Inicio","alias":"inicio","description":null,"nav_item_type":1,"nav_item_type_id":3,"nav_id":2,"timestamp_create":1480446925,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":3}', 'cms_nav_item', 3),
(8, 1, 0, 1, 0, 1480446954, '{"tableName":"cms_nav","action":"update","row":2}', '{"id":2,"nav_container_id":1,"parent_nav_id":0,"sort_index":2,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 2),
(9, 1, 0, 1, 0, 1480446956, '{"tableName":"cms_nav","action":"update","row":2}', '{"id":2,"nav_container_id":1,"parent_nav_id":0,"sort_index":2,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 2),
(10, 1, 1, 0, 0, 1480447173, '{"tableName":"cms_nav","action":"insert","row":3}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":3,"id":3}', 'cms_nav', 3),
(11, 1, 1, 0, 0, 1480447173, '{"tableName":"cms_nav_item","action":"insert","row":4}', '{"lang_id":"2","title":"Inicio","alias":"inicio","description":null,"nav_item_type":1,"nav_item_type_id":4,"nav_id":3,"timestamp_create":1480447173,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":4}', 'cms_nav_item', 4),
(12, 1, 0, 1, 0, 1480447205, '{"tableName":"cms_nav","action":"update","row":3}', '{"id":3,"nav_container_id":1,"parent_nav_id":0,"sort_index":3,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 3),
(13, 1, 0, 1, 0, 1480447207, '{"tableName":"cms_nav","action":"update","row":3}', '{"id":3,"nav_container_id":1,"parent_nav_id":0,"sort_index":3,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 3),
(14, 1, 0, 1, 0, 1480447211, '{"tableName":"cms_nav","action":"update","row":3}', '{"id":3,"nav_container_id":1,"parent_nav_id":0,"sort_index":3,"is_deleted":0,"is_hidden":0,"is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 3),
(15, 1, 1, 0, 0, 1480447250, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":1,"pageTitle":"Inicio","blockName":"Mesa"}', '[]', 'cms_nav_item_page_block_item', 1),
(16, 1, 0, 1, 0, 1480447306, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":1,"pageTitle":"Inicio","blockName":"Mesa"}', '[]', 'cms_nav_item_page_block_item', 1),
(17, 1, 1, 0, 0, 1480447368, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":2,"pageTitle":"Inicio","blockName":"Botón de enlace"}', '[]', 'cms_nav_item_page_block_item', 2),
(18, 1, 0, 1, 0, 1480447415, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":2,"pageTitle":"Inicio","blockName":"Botón de enlace"}', '[]', 'cms_nav_item_page_block_item', 2),
(19, 1, 0, 1, 0, 1480447497, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":2,"pageTitle":"Inicio","blockName":"Botón de enlace"}', '[]', 'cms_nav_item_page_block_item', 2),
(20, 1, 1, 0, 0, 1480447504, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":3,"pageTitle":"Inicio","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 3),
(21, 1, 1, 0, 0, 1480447526, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":4,"pageTitle":"Inicio","blockName":"Vídeo"}', '[]', 'cms_nav_item_page_block_item', 4),
(22, 1, 1, 0, 0, 1480447532, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":5,"pageTitle":"Inicio","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 5),
(23, 1, 0, 1, 0, 1480447624, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":4,"pageTitle":"Inicio","blockName":"Vídeo"}', '[]', 'cms_nav_item_page_block_item', 4),
(24, 1, 0, 1, 0, 1480447685, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":5,"pageTitle":"Inicio","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 5),
(25, 1, 0, 1, 0, 1480447721, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":3,"pageTitle":"Inicio","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 3),
(26, 1, 0, 1, 0, 1480448077, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":3,"pageTitle":"Inicio","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 3),
(27, 1, 1, 0, 0, 1480448852, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":6,"pageTitle":"Inicio","blockName":"Citar"}', '[]', 'cms_nav_item_page_block_item', 6),
(28, 1, 1, 0, 0, 1480448864, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":7,"pageTitle":"Inicio","blockName":"Editor de texto"}', '[]', 'cms_nav_item_page_block_item', 7),
(29, 1, 1, 0, 0, 1480448878, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":8,"pageTitle":"Inicio","blockName":"Lista"}', '[]', 'cms_nav_item_page_block_item', 8),
(30, 1, 1, 0, 0, 1480448960, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":9,"pageTitle":"Inicio","blockName":"Mapa"}', '[]', 'cms_nav_item_page_block_item', 9),
(31, 1, 1, 0, 0, 1480448986, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":10,"pageTitle":"Inicio","blockName":"Audio"}', '[]', 'cms_nav_item_page_block_item', 10),
(32, 1, 1, 0, 0, 1480448999, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":11,"pageTitle":"Inicio","blockName":"Formar"}', '[]', 'cms_nav_item_page_block_item', 11),
(33, 1, 0, 0, 1, 1480510668, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":11,"pageTitle":"Inicio","blockName":"Formar"}', '[]', 'cms_nav_item_page_block_item', 11),
(34, 1, 0, 0, 1, 1480510671, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":10,"pageTitle":"Inicio","blockName":"Audio"}', '[]', 'cms_nav_item_page_block_item', 10),
(35, 1, 0, 0, 1, 1480510674, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":9,"pageTitle":"Inicio","blockName":"Mapa"}', '[]', 'cms_nav_item_page_block_item', 9),
(36, 1, 0, 0, 1, 1480510677, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":8,"pageTitle":"Inicio","blockName":"Lista"}', '[]', 'cms_nav_item_page_block_item', 8),
(37, 1, 0, 0, 1, 1480510681, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":7,"pageTitle":"Inicio","blockName":"Editor de texto"}', '[]', 'cms_nav_item_page_block_item', 7),
(38, 1, 0, 0, 1, 1480510683, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":6,"pageTitle":"Inicio","blockName":"Citar"}', '[]', 'cms_nav_item_page_block_item', 6),
(39, 1, 0, 0, 1, 1480510693, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":4,"pageTitle":"Inicio","blockName":"Vídeo"}', '[]', 'cms_nav_item_page_block_item', 4),
(40, 1, 0, 0, 1, 1480510693, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":5,"pageTitle":"Inicio","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 5),
(41, 1, 0, 0, 1, 1480510693, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":3,"pageTitle":"Inicio","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 3),
(42, 1, 0, 0, 1, 1480510695, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":1,"pageTitle":"Inicio","blockName":"Mesa"}', '[]', 'cms_nav_item_page_block_item', 1),
(43, 1, 0, 0, 1, 1480510707, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":2,"pageTitle":"Inicio","blockName":"Botón de enlace"}', '[]', 'cms_nav_item_page_block_item', 2),
(44, 1, 1, 0, 0, 1480510770, '{"tableName":"cms_nav","action":"insert","row":4}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":4,"id":4}', 'cms_nav', 4),
(45, 1, 1, 0, 0, 1480510770, '{"tableName":"cms_nav_item","action":"insert","row":5}', '{"lang_id":"2","title":"Nosotros","alias":"nosotros","description":null,"nav_item_type":1,"nav_item_type_id":5,"nav_id":4,"timestamp_create":1480510770,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":5}', 'cms_nav_item', 5),
(46, 1, 0, 1, 0, 1480510775, '{"tableName":"cms_nav","action":"update","row":4}', '{"id":4,"nav_container_id":1,"parent_nav_id":0,"sort_index":4,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 4),
(47, 1, 0, 1, 0, 1480510775, '{"tableName":"cms_nav","action":"update","row":4}', '{"id":4,"nav_container_id":1,"parent_nav_id":0,"sort_index":4,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 4),
(48, 1, 1, 0, 0, 1480510816, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(49, 1, 1, 0, 0, 1480510862, '{"tableName":"cms_nav","action":"insert","row":5}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":5,"id":5}', 'cms_nav', 5),
(50, 1, 1, 0, 0, 1480510862, '{"tableName":"cms_nav_item","action":"insert","row":6}', '{"lang_id":"2","title":"Contactos","alias":"contactos","description":null,"nav_item_type":1,"nav_item_type_id":6,"nav_id":5,"timestamp_create":1480510862,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":6}', 'cms_nav_item', 6),
(51, 1, 0, 1, 0, 1480510868, '{"tableName":"cms_nav","action":"update","row":5}', '{"id":5,"nav_container_id":1,"parent_nav_id":0,"sort_index":5,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 5),
(52, 1, 0, 1, 0, 1480510869, '{"tableName":"cms_nav","action":"update","row":5}', '{"id":5,"nav_container_id":1,"parent_nav_id":0,"sort_index":5,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 5),
(53, 1, 0, 1, 0, 1480510869, '{"tableName":"cms_nav","action":"update","row":5}', '{"id":5,"nav_container_id":1,"parent_nav_id":0,"sort_index":5,"is_deleted":0,"is_hidden":0,"is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 5),
(54, 1, 0, 1, 0, 1480510876, '{"tableName":"cms_nav","action":"update","row":5}', '{"id":5,"nav_container_id":1,"parent_nav_id":0,"sort_index":5,"is_deleted":0,"is_hidden":"1","is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 5),
(55, 1, 0, 1, 0, 1480510877, '{"tableName":"cms_nav","action":"update","row":5}', '{"id":5,"nav_container_id":1,"parent_nav_id":0,"sort_index":5,"is_deleted":0,"is_hidden":1,"is_offline":"1","is_home":1,"is_draft":0}', 'cms_nav', 5),
(56, 1, 0, 1, 0, 1480510878, '{"tableName":"cms_nav","action":"update","row":5}', '{"id":5,"nav_container_id":1,"parent_nav_id":0,"sort_index":5,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":1,"is_draft":0}', 'cms_nav', 5),
(57, 1, 0, 1, 0, 1480510879, '{"tableName":"cms_nav","action":"update","row":5}', '{"id":5,"nav_container_id":1,"parent_nav_id":0,"sort_index":5,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 5),
(58, 1, 0, 1, 0, 1480510886, '{"tableName":"cms_nav","action":"update","row":3}', '{"id":3,"nav_container_id":1,"parent_nav_id":0,"sort_index":3,"is_deleted":0,"is_hidden":0,"is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 3),
(59, 1, 1, 0, 0, 1480534180, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":13,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 13),
(60, 1, 0, 1, 0, 1480534188, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":13,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 13),
(61, 1, 0, 1, 0, 1480534304, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":13,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 13),
(62, 1, 1, 0, 0, 1480534992, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":14,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 14),
(63, 1, 0, 1, 0, 1480535002, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":14,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 14),
(64, 1, 0, 1, 0, 1480535168, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":14,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 14),
(65, 1, 0, 0, 1, 1480535172, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":14,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 14),
(66, 1, 0, 1, 0, 1480535187, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":13,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 13),
(67, 1, 0, 1, 0, 1480535299, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":13,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 13),
(68, 1, 0, 1, 0, 1480535452, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(69, 1, 1, 0, 0, 1480535514, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":15,"pageTitle":"Contactos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 15),
(70, 1, 0, 1, 0, 1480535576, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":15,"pageTitle":"Contactos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 15),
(71, 1, 0, 1, 0, 1480535811, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":15,"pageTitle":"Contactos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 15),
(72, 1, 1, 0, 0, 1480535950, '{"tableName":"cms_nav","action":"insert","row":6}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":6,"id":6}', 'cms_nav', 6),
(73, 1, 1, 0, 0, 1480535950, '{"tableName":"cms_nav_item","action":"insert","row":7}', '{"lang_id":"2","title":"Alquiler de equipos","alias":"alquiler-de-equipos","description":null,"nav_item_type":1,"nav_item_type_id":7,"nav_id":6,"timestamp_create":1480535950,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":7}', 'cms_nav_item', 7),
(74, 1, 0, 1, 0, 1480535955, '{"tableName":"cms_nav","action":"update","row":6}', '{"id":6,"nav_container_id":1,"parent_nav_id":0,"sort_index":6,"is_deleted":0,"is_hidden":"0","is_offline":1,"is_home":0,"is_draft":0}', 'cms_nav', 6),
(75, 1, 0, 1, 0, 1480535956, '{"tableName":"cms_nav","action":"update","row":6}', '{"id":6,"nav_container_id":1,"parent_nav_id":0,"sort_index":6,"is_deleted":0,"is_hidden":0,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 6),
(76, 1, 1, 0, 0, 1480535964, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":16,"pageTitle":"Alquiler de equipos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 16),
(77, 1, 0, 1, 0, 1480536015, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":16,"pageTitle":"Alquiler de equipos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 16),
(78, 1, 1, 0, 0, 1480536149, '{"tableName":"cms_nav","action":"insert","row":7}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":7,"id":7}', 'cms_nav', 7),
(79, 1, 1, 0, 0, 1480536149, '{"tableName":"cms_nav_item","action":"insert","row":8}', '{"lang_id":"2","title":"Ox\\u00edgeno medicinal","alias":"oxigeno-medicinal","description":null,"nav_item_type":1,"nav_item_type_id":8,"nav_id":7,"timestamp_create":1480536149,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":8}', 'cms_nav_item', 8),
(80, 1, 1, 0, 0, 1480536162, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":17,"pageTitle":"Oxígeno medicinal","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 17),
(81, 1, 0, 1, 0, 1480536190, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":17,"pageTitle":"Oxígeno medicinal","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 17),
(82, 1, 0, 1, 0, 1480536202, '{"tableName":"cms_nav","action":"update","row":7}', '{"id":7,"nav_container_id":1,"parent_nav_id":0,"sort_index":7,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 7),
(83, 1, 0, 1, 0, 1480536203, '{"tableName":"cms_nav","action":"update","row":7}', '{"id":7,"nav_container_id":1,"parent_nav_id":0,"sort_index":7,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 7),
(84, 1, 1, 0, 0, 1480536300, '{"tableName":"cms_nav","action":"insert","row":8}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":8,"id":8}', 'cms_nav', 8),
(85, 1, 1, 0, 0, 1480536300, '{"tableName":"cms_nav_item","action":"insert","row":9}', '{"lang_id":"2","title":"Oxigeno industrial","alias":"oxigeno-industrial","description":null,"nav_item_type":1,"nav_item_type_id":9,"nav_id":8,"timestamp_create":1480536300,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":9}', 'cms_nav_item', 9),
(86, 1, 0, 1, 0, 1480536307, '{"tableName":"cms_nav","action":"update","row":8}', '{"id":8,"nav_container_id":1,"parent_nav_id":0,"sort_index":8,"is_deleted":0,"is_hidden":"0","is_offline":1,"is_home":0,"is_draft":0}', 'cms_nav', 8),
(87, 1, 0, 1, 0, 1480536307, '{"tableName":"cms_nav","action":"update","row":8}', '{"id":8,"nav_container_id":1,"parent_nav_id":0,"sort_index":8,"is_deleted":0,"is_hidden":0,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 8),
(88, 1, 1, 0, 0, 1480536311, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":18,"pageTitle":"Oxigeno industrial","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 18),
(89, 1, 0, 1, 0, 1480536359, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":18,"pageTitle":"Oxigeno industrial","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 18),
(90, 1, 1, 0, 0, 1480536421, '{"tableName":"cms_nav","action":"insert","row":9}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":9,"id":9}', 'cms_nav', 9),
(91, 1, 1, 0, 0, 1480536421, '{"tableName":"cms_nav_item","action":"insert","row":10}', '{"lang_id":"2","title":"Accesorios","alias":"accesorios","description":null,"nav_item_type":1,"nav_item_type_id":10,"nav_id":9,"timestamp_create":1480536421,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":10}', 'cms_nav_item', 10),
(92, 1, 1, 0, 0, 1480536429, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":19,"pageTitle":"Accesorios","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 19),
(93, 1, 0, 1, 0, 1480536453, '{"tableName":"cms_nav","action":"update","row":9}', '{"id":9,"nav_container_id":1,"parent_nav_id":0,"sort_index":9,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 9),
(94, 1, 0, 1, 0, 1480536453, '{"tableName":"cms_nav","action":"update","row":9}', '{"id":9,"nav_container_id":1,"parent_nav_id":0,"sort_index":9,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 9),
(95, 1, 0, 1, 0, 1480536461, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":19,"pageTitle":"Accesorios","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 19),
(96, 1, 1, 0, 0, 1480536524, '{"tableName":"cms_nav","action":"insert","row":10}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":10,"id":10}', 'cms_nav', 10),
(97, 1, 1, 0, 0, 1480536524, '{"tableName":"cms_nav_item","action":"insert","row":11}', '{"lang_id":"2","title":"Equipos ortop\\u00e9dicos","alias":"equipos-ortopedicos","description":null,"nav_item_type":1,"nav_item_type_id":11,"nav_id":10,"timestamp_create":1480536524,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":11}', 'cms_nav_item', 11),
(98, 1, 0, 1, 0, 1480536539, '{"tableName":"cms_nav_item","action":"update","row":9}', '{"id":9,"nav_id":8,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"9","create_user_id":1,"update_user_id":1,"timestamp_create":1480536300,"timestamp_update":1480536539,"title":"Ox\\u00edgeno industrial","alias":"oxigeno-industrial","description":"","keywords":"","title_tag":""}', 'cms_nav_item', 9),
(99, 1, 0, 1, 0, 1480536548, '{"tableName":"cms_nav","action":"update","row":10}', '{"id":10,"nav_container_id":1,"parent_nav_id":0,"sort_index":10,"is_deleted":0,"is_hidden":"0","is_offline":1,"is_home":0,"is_draft":0}', 'cms_nav', 10),
(100, 1, 0, 1, 0, 1480536549, '{"tableName":"cms_nav","action":"update","row":10}', '{"id":10,"nav_container_id":1,"parent_nav_id":0,"sort_index":10,"is_deleted":0,"is_hidden":0,"is_offline":1,"is_home":1,"is_draft":0}', 'cms_nav', 10),
(101, 1, 0, 1, 0, 1480536553, '{"tableName":"cms_nav","action":"update","row":3}', '{"id":3,"nav_container_id":1,"parent_nav_id":0,"sort_index":3,"is_deleted":0,"is_hidden":0,"is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 3),
(102, 1, 1, 0, 0, 1480536562, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":20,"pageTitle":"Equipos ortopédicos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 20),
(103, 1, 0, 1, 0, 1480536605, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":20,"pageTitle":"Equipos ortopédicos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 20),
(104, 1, 0, 1, 0, 1480536613, '{"tableName":"cms_nav","action":"update","row":10}', '{"id":10,"nav_container_id":1,"parent_nav_id":0,"sort_index":10,"is_deleted":0,"is_hidden":0,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 10),
(105, 1, 1, 0, 0, 1480536677, '{"tableName":"cms_nav","action":"insert","row":11}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":11,"id":11}', 'cms_nav', 11),
(106, 1, 1, 0, 0, 1480536677, '{"tableName":"cms_nav_item","action":"insert","row":12}', '{"lang_id":"2","title":"Extintores","alias":"extintores","description":null,"nav_item_type":1,"nav_item_type_id":12,"nav_id":11,"timestamp_create":1480536677,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":12}', 'cms_nav_item', 12),
(107, 1, 0, 1, 0, 1480536682, '{"tableName":"cms_nav","action":"update","row":11}', '{"id":11,"nav_container_id":1,"parent_nav_id":0,"sort_index":11,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 11),
(108, 1, 0, 1, 0, 1480536683, '{"tableName":"cms_nav","action":"update","row":11}', '{"id":11,"nav_container_id":1,"parent_nav_id":0,"sort_index":11,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 11),
(109, 1, 1, 0, 0, 1480536687, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":21,"pageTitle":"Extintores","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 21),
(110, 1, 0, 1, 0, 1480536743, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":21,"pageTitle":"Extintores","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 21),
(111, 1, 1, 0, 0, 1480536849, '{"tableName":"cms_nav","action":"insert","row":12}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":12,"id":12}', 'cms_nav', 12),
(112, 1, 1, 0, 0, 1480536849, '{"tableName":"cms_nav_item","action":"insert","row":13}', '{"lang_id":"2","title":"Novedades y Ofertas","alias":"novedades-y-ofertas","description":null,"nav_item_type":1,"nav_item_type_id":13,"nav_id":12,"timestamp_create":1480536849,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":13}', 'cms_nav_item', 13),
(113, 1, 0, 1, 0, 1480536856, '{"tableName":"cms_nav","action":"update","row":12}', '{"id":12,"nav_container_id":1,"parent_nav_id":0,"sort_index":12,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 12),
(114, 1, 0, 1, 0, 1480536856, '{"tableName":"cms_nav","action":"update","row":12}', '{"id":12,"nav_container_id":1,"parent_nav_id":0,"sort_index":12,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 12),
(115, 1, 0, 1, 0, 1480536857, '{"tableName":"cms_nav","action":"update","row":12}', '{"id":12,"nav_container_id":1,"parent_nav_id":0,"sort_index":12,"is_deleted":0,"is_hidden":0,"is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 12),
(116, 1, 0, 1, 0, 1480536861, '{"tableName":"cms_nav","action":"update","row":3}', '{"id":3,"nav_container_id":1,"parent_nav_id":0,"sort_index":3,"is_deleted":0,"is_hidden":0,"is_offline":0,"is_home":1,"is_draft":0}', 'cms_nav', 3),
(117, 1, 1, 0, 0, 1480536878, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":22,"pageTitle":"Novedades y Ofertas","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 22),
(118, 1, 0, 1, 0, 1480536933, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":22,"pageTitle":"Novedades y Ofertas","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 22),
(119, 1, 1, 0, 0, 1480536939, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":23,"pageTitle":"Novedades y Ofertas","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 23),
(120, 1, 0, 1, 0, 1480536957, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":23,"pageTitle":"Novedades y Ofertas","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 23),
(121, 1, 1, 0, 0, 1480536970, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":24,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 24),
(122, 1, 0, 1, 0, 1480537049, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":24,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 24),
(123, 1, 1, 0, 0, 1480542658, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(124, 1, 0, 0, 1, 1480542668, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":24,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 24),
(125, 1, 1, 0, 0, 1480542693, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":26,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 26),
(126, 1, 1, 0, 0, 1480542702, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":27,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 27),
(127, 1, 0, 1, 0, 1480542748, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":26,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 26),
(128, 1, 0, 1, 0, 1480542871, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":26,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 26),
(129, 1, 0, 1, 0, 1480542939, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(130, 1, 0, 1, 0, 1480542965, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":27,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 27),
(131, 1, 0, 1, 0, 1480543123, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":20,"pageTitle":"Equipos ortopédicos","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 20),
(132, 1, 0, 1, 0, 1480543338, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":27,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 27),
(133, 1, 0, 1, 0, 1480557699, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":23,"pageTitle":"Novedades y Ofertas","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 23),
(134, 1, 1, 0, 0, 1480558438, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":28,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 28),
(135, 1, 0, 0, 1, 1480558458, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":28,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 28),
(136, 1, 1, 0, 0, 1480558471, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":29,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 29),
(137, 1, 1, 0, 0, 1480558471, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":30,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 30),
(138, 1, 1, 0, 0, 1480558472, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":31,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 31),
(139, 1, 0, 1, 0, 1480558719, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480558719,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"","title_tag":""}', 'cms_nav_item', 4),
(140, 1, 0, 1, 0, 1480559871, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480559871,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed bolivia, Ox\\u00edgeno medicinal, ox\\u00edgeno industrial, equipos de ortoped\\u00eda, extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(141, 1, 0, 1, 0, 1480560125, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560125,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Ox\\u00edgeno medicinal, Ox\\u00edgeno industrial, Equipos ortopedicos, Extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(142, 1, 0, 1, 0, 1480560246, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560246,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Oxigeno industrial, Equipos ortopedicos, Extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(143, 1, 0, 1, 0, 1480560298, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560298,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Oxigeno Industrial, Equipos ortopedicos, Extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(144, 1, 0, 1, 0, 1480560348, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560348,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, oxigeno industrial, Equipos ortopedicos, Extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(145, 1, 0, 1, 0, 1480560375, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560375,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Ox\\u00edgeno industrial, Equipos ortopedicos, Extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(146, 1, 0, 1, 0, 1480560430, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560430,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Oxigeno medicinal industrial, Equipos ortopedicos, Extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(147, 1, 0, 1, 0, 1480560454, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560454,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(148, 1, 0, 1, 0, 1480560508, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560508,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Alquiler de equipos","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(149, 1, 0, 1, 0, 1480560560, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560560,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Oxigenoterapia a domicilio","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(150, 1, 0, 1, 0, 1480560621, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560621,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Oxigenoterapia a domicilio, Oxigeno industrial","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(151, 1, 0, 1, 0, 1480560657, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480560657,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Oxigenoterapia a domicilio","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(152, 1, 0, 1, 0, 1480560794, '{"tableName":"cms_nav_item","action":"update","row":5}', '{"id":5,"nav_id":4,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"5","create_user_id":1,"update_user_id":1,"timestamp_create":1480510770,"timestamp_update":1480560794,"title":"Nosotros","alias":"nosotros","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Oxigenoterapia a domicilio","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 5),
(153, 1, 0, 1, 0, 1480560884, '{"tableName":"cms_nav_item","action":"update","row":5}', '{"id":5,"nav_id":4,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"5","create_user_id":1,"update_user_id":1,"timestamp_create":1480510770,"timestamp_update":1480560883,"title":"Nosotros","alias":"nosotros","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Nosotros","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 5),
(154, 1, 0, 1, 0, 1480560935, '{"tableName":"cms_nav_item","action":"update","row":5}', '{"id":5,"nav_id":4,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"5","create_user_id":1,"update_user_id":1,"timestamp_create":1480510770,"timestamp_update":1480560935,"title":"Nosotros","alias":"nosotros","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Nosotros, Video presentaci\\u00f3n, Misi\\u00f3n, Visi\\u00f3n","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 5),
(155, 1, 0, 1, 0, 1480561043, '{"tableName":"cms_nav_item","action":"update","row":6}', '{"id":6,"nav_id":5,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"6","create_user_id":1,"update_user_id":1,"timestamp_create":1480510862,"timestamp_update":1480561043,"title":"Contactos","alias":"contactos","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Cont\\u00e1ctenos, Ub\\u00edquenos","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 6),
(156, 1, 0, 1, 0, 1480561073, '{"tableName":"cms_nav_item","action":"update","row":6}', '{"id":6,"nav_id":5,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"6","create_user_id":1,"update_user_id":1,"timestamp_create":1480510862,"timestamp_update":1480561073,"title":"Contactos","alias":"contactos","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Cont\\u00e1ctenos, Ub\\u00edquenos, OXIMED","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 6),
(157, 1, 0, 1, 0, 1480561135, '{"tableName":"cms_nav_item","action":"update","row":6}', '{"id":6,"nav_id":5,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"6","create_user_id":1,"update_user_id":1,"timestamp_create":1480510862,"timestamp_update":1480561135,"title":"Contactos","alias":"contactos","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Cont\\u00e1ctenos","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 6),
(158, 1, 0, 1, 0, 1480561205, '{"tableName":"cms_nav_item","action":"update","row":7}', '{"id":7,"nav_id":6,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"7","create_user_id":1,"update_user_id":1,"timestamp_create":1480535950,"timestamp_update":1480561205,"title":"Alquiler de equipos","alias":"alquiler-de-equipos","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"ALQUILER DE OXIGENOTERAPIA,","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 7),
(159, 1, 0, 1, 0, 1480561298, '{"tableName":"cms_nav_item","action":"update","row":7}', '{"id":7,"nav_id":6,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"7","create_user_id":1,"update_user_id":1,"timestamp_create":1480535950,"timestamp_update":1480561298,"title":"Alquiler de equipos","alias":"alquiler-de-equipos","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"ALQUILER DE OXIGENOTERAPIA, OXIMED","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 7),
(160, 1, 0, 1, 0, 1480561346, '{"tableName":"cms_nav_item","action":"update","row":7}', '{"id":7,"nav_id":6,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"7","create_user_id":1,"update_user_id":1,"timestamp_create":1480535950,"timestamp_update":1480561346,"title":"Alquiler de equipos","alias":"alquiler-de-equipos","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"ALQUILER DE OXIGENOTERAPIA, OXIMED, terapias respiratorias domiciliarias, equipos de oxigeno","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 7),
(161, 1, 0, 1, 0, 1480561994, '{"tableName":"cms_nav_item","action":"update","row":8}', '{"id":8,"nav_id":7,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"8","create_user_id":1,"update_user_id":1,"timestamp_create":1480536149,"timestamp_update":1480561994,"title":"Ox\\u00edgeno medicinal","alias":"oxigeno-medicinal","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oxinoterapia, administraci\\u00f3n de ox\\u00edgeno, Equipos de Oxigeno Medicinal","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 8),
(162, 1, 0, 1, 0, 1480562085, '{"tableName":"cms_nav_item","action":"update","row":9}', '{"id":9,"nav_id":8,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"9","create_user_id":1,"update_user_id":1,"timestamp_create":1480536300,"timestamp_update":1480562085,"title":"Ox\\u00edgeno industrial","alias":"oxigeno-industrial","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"OXIGENO INDUSTRIAL","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 9),
(163, 1, 0, 1, 0, 1480562152, '{"tableName":"cms_nav_item","action":"update","row":10}', '{"id":10,"nav_id":9,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"10","create_user_id":1,"update_user_id":1,"timestamp_create":1480536421,"timestamp_update":1480562152,"title":"Accesorios","alias":"accesorios","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"ACCESORIOS","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 10),
(164, 1, 0, 1, 0, 1480562226, '{"tableName":"cms_nav_item","action":"update","row":11}', '{"id":11,"nav_id":10,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"11","create_user_id":1,"update_user_id":1,"timestamp_create":1480536524,"timestamp_update":1480562226,"title":"Equipos ortop\\u00e9dicos","alias":"equipos-ortopedicos","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"EQUIPOS ORTOP\\u00c9DICOS","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 11),
(165, 1, 0, 1, 0, 1480562279, '{"tableName":"cms_nav_item","action":"update","row":12}', '{"id":12,"nav_id":11,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"12","create_user_id":1,"update_user_id":1,"timestamp_create":1480536677,"timestamp_update":1480562279,"title":"Extintores","alias":"extintores","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"EXTINTORES","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 12),
(166, 1, 0, 1, 0, 1480562327, '{"tableName":"cms_nav_item","action":"update","row":13}', '{"id":13,"nav_id":12,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"13","create_user_id":1,"update_user_id":1,"timestamp_create":1480536849,"timestamp_update":1480562327,"title":"Novedades y Ofertas","alias":"novedades-y-ofertas","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Novedades y Ofertas","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 13),
(167, 1, 0, 1, 0, 1480562437, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1480562437,"title":"Inicio","alias":"inicio","description":"OXIMED BOLIVIA - Ox\\u00edgeno Medicinal Industrial","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Oxigenoterapia a domicilio","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(168, 1, 0, 1, 0, 1480602427, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":22,"pageTitle":"Novedades y Ofertas","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 22),
(169, 2, 1, 0, 0, 1480610435, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":32,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 32),
(170, 2, 1, 0, 0, 1480610435, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":33,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 33),
(171, 2, 1, 0, 0, 1480610436, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":34,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 34),
(172, 1, 0, 0, 1, 1480610655, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":26,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 26),
(173, 1, 1, 0, 0, 1480610666, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":35,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 35),
(174, 1, 0, 0, 1, 1480610674, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":27,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 27),
(175, 1, 1, 0, 0, 1480610678, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":36,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 36),
(176, 1, 0, 0, 1, 1480610710, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":30,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 30),
(177, 1, 0, 0, 1, 1480610710, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":31,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 31),
(178, 1, 0, 0, 1, 1480610710, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":29,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 29),
(179, 1, 0, 0, 1, 1480610715, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":33,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 33),
(180, 1, 0, 0, 1, 1480610715, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":34,"pageTitle":"Novedades y Ofertas","blockName":"Texto"}', '[]', 'cms_nav_item_page_block_item', 34),
(181, 1, 0, 0, 1, 1480610715, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":32,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 32),
(182, 1, 0, 1, 0, 1480610835, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(183, 1, 0, 1, 0, 1480610841, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(184, 1, 0, 1, 0, 1480610923, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":35,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 35),
(185, 1, 0, 1, 0, 1480610931, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":35,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 35),
(186, 1, 0, 1, 0, 1480610938, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":36,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 36),
(187, 1, 1, 0, 0, 1480610949, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":37,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 37),
(188, 1, 1, 0, 0, 1480610968, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":38,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 38),
(189, 1, 0, 0, 1, 1480611014, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":37,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 37),
(190, 1, 1, 0, 0, 1480611028, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":39,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 39);
INSERT INTO `cms_log` (`id`, `user_id`, `is_insertion`, `is_update`, `is_deletion`, `timestamp`, `message`, `data_json`, `table_name`, `row_id`) VALUES
(191, 1, 1, 0, 0, 1480611032, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":40,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 40),
(192, 1, 1, 0, 0, 1480611036, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":41,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 41),
(193, 1, 0, 1, 0, 1480611120, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":38,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 38),
(194, 1, 0, 1, 0, 1480611215, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(195, 1, 0, 1, 0, 1480611311, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":39,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 39),
(196, 1, 0, 1, 0, 1480611337, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":38,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 38),
(197, 1, 0, 1, 0, 1480611361, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":40,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 40),
(198, 1, 0, 1, 0, 1480611372, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":41,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 41),
(199, 1, 0, 1, 0, 1480611517, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(200, 1, 0, 1, 0, 1480611597, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(201, 1, 0, 1, 0, 1480630023, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":38,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 38),
(202, 1, 0, 1, 0, 1480630102, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(203, 1, 0, 1, 0, 1480630114, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":35,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 35),
(204, 1, 0, 1, 0, 1480630118, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(205, 1, 0, 1, 0, 1480630159, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":41,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 41),
(206, 1, 0, 1, 0, 1480630162, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":40,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 40),
(207, 1, 0, 1, 0, 1480630164, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":39,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 39),
(208, 1, 1, 0, 0, 1480631011, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":42,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 42),
(209, 1, 1, 0, 0, 1480631011, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":43,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 43),
(210, 1, 1, 0, 0, 1480631011, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":44,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 44),
(211, 1, 1, 0, 0, 1480631011, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":45,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 45),
(212, 1, 1, 0, 0, 1480631011, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":46,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 46),
(213, 1, 1, 0, 0, 1480631011, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":47,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 47),
(214, 1, 1, 0, 0, 1480631011, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":48,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 48),
(215, 1, 0, 1, 0, 1480637606, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":38,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 38),
(216, 1, 0, 0, 1, 1480637881, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":38,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 38),
(217, 1, 0, 1, 0, 1480637933, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":48,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 48),
(218, 1, 0, 1, 0, 1480693920, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":42,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 42),
(219, 1, 0, 1, 0, 1480709779, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":13,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 13),
(220, 1, 0, 1, 0, 1480709794, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":13,"pageTitle":"Inicio","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 13),
(221, 1, 0, 1, 0, 1480709938, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":17,"pageTitle":"Oxígeno medicinal","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 17),
(222, 1, 0, 1, 0, 1480710169, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":25,"pageTitle":"Novedades y Ofertas","blockName":"Diseño"}', '[]', 'cms_nav_item_page_block_item', 25),
(223, 1, 1, 0, 0, 1480710248, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":49,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 49),
(224, 1, 0, 0, 1, 1480710263, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":49,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 49),
(225, 2, 1, 0, 0, 1480714657, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":50,"pageTitle":"Novedades y Ofertas","blockName":"Audio"}', '[]', 'cms_nav_item_page_block_item', 50),
(226, 2, 0, 0, 1, 1480714664, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":50,"pageTitle":"Novedades y Ofertas","blockName":"Audio"}', '[]', 'cms_nav_item_page_block_item', 50),
(227, 2, 1, 0, 0, 1480714706, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":51,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 51),
(228, 2, 0, 0, 1, 1480714710, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":51,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 51),
(229, 1, 0, 1, 0, 1481294431, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(230, 1, 0, 1, 0, 1481294473, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(231, 1, 0, 1, 0, 1481294649, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(232, 1, 0, 1, 0, 1481294781, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(233, 1, 0, 1, 0, 1481294786, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(234, 1, 0, 1, 0, 1481294789, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(235, 1, 0, 1, 0, 1481294851, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(236, 1, 0, 1, 0, 1481322674, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(237, 1, 0, 1, 0, 1481322691, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(238, 1, 0, 1, 0, 1481322701, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":12,"pageTitle":"Nosotros","blockName":"HTML"}', '[]', 'cms_nav_item_page_block_item', 12),
(239, 1, 0, 1, 0, 1481505614, '{"tableName":"cms_nav_item","action":"update","row":4}', '{"id":4,"nav_id":3,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"4","create_user_id":1,"update_user_id":1,"timestamp_create":1480447173,"timestamp_update":1481505614,"title":"Ox\\u00edgeno Medicinal Domiciliario","alias":"inicio","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Oxigenoterapia a domicilio","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 4),
(240, 1, 0, 1, 0, 1481505673, '{"tableName":"cms_nav_item","action":"update","row":5}', '{"id":5,"nav_id":4,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"5","create_user_id":1,"update_user_id":1,"timestamp_create":1480510770,"timestamp_update":1481505673,"title":"Nosotros","alias":"nosotros","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"Nosotros, Video presentaci\\u00f3n, Misi\\u00f3n, Visi\\u00f3n","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 5),
(241, 1, 0, 1, 0, 1481505706, '{"tableName":"cms_nav_item","action":"update","row":6}', '{"id":6,"nav_id":5,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"6","create_user_id":1,"update_user_id":1,"timestamp_create":1480510862,"timestamp_update":1481505706,"title":"Contactos","alias":"contactos","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"Cont\\u00e1ctenos","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 6),
(242, 1, 0, 1, 0, 1481505723, '{"tableName":"cms_nav_item","action":"update","row":7}', '{"id":7,"nav_id":6,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"7","create_user_id":1,"update_user_id":1,"timestamp_create":1480535950,"timestamp_update":1481505723,"title":"Alquiler de equipos","alias":"alquiler-de-equipos","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"ALQUILER DE OXIGENOTERAPIA, OXIMED, terapias respiratorias domiciliarias, equipos de oxigeno","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 7),
(243, 1, 0, 1, 0, 1481505734, '{"tableName":"cms_nav_item","action":"update","row":8}', '{"id":8,"nav_id":7,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"8","create_user_id":1,"update_user_id":1,"timestamp_create":1480536149,"timestamp_update":1481505734,"title":"Ox\\u00edgeno medicinal","alias":"oxigeno-medicinal","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"Oxinoterapia, administraci\\u00f3n de ox\\u00edgeno, Equipos de Oxigeno Medicinal","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 8),
(244, 1, 0, 1, 0, 1481505744, '{"tableName":"cms_nav_item","action":"update","row":9}', '{"id":9,"nav_id":8,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"9","create_user_id":1,"update_user_id":1,"timestamp_create":1480536300,"timestamp_update":1481505743,"title":"Ox\\u00edgeno industrial","alias":"oxigeno-industrial","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"OXIGENO INDUSTRIAL","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 9),
(245, 1, 0, 1, 0, 1481505753, '{"tableName":"cms_nav_item","action":"update","row":10}', '{"id":10,"nav_id":9,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"10","create_user_id":1,"update_user_id":1,"timestamp_create":1480536421,"timestamp_update":1481505753,"title":"Accesorios","alias":"accesorios","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"ACCESORIOS","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 10),
(246, 1, 0, 1, 0, 1481505762, '{"tableName":"cms_nav_item","action":"update","row":11}', '{"id":11,"nav_id":10,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"11","create_user_id":1,"update_user_id":1,"timestamp_create":1480536524,"timestamp_update":1481505762,"title":"Equipos ortop\\u00e9dicos","alias":"equipos-ortopedicos","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"EQUIPOS ORTOP\\u00c9DICOS","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 11),
(247, 1, 0, 1, 0, 1481505771, '{"tableName":"cms_nav_item","action":"update","row":12}', '{"id":12,"nav_id":11,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"12","create_user_id":1,"update_user_id":1,"timestamp_create":1480536677,"timestamp_update":1481505771,"title":"Extintores","alias":"extintores","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"EXTINTORES","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 12),
(248, 1, 0, 1, 0, 1481505781, '{"tableName":"cms_nav_item","action":"update","row":13}', '{"id":13,"nav_id":12,"lang_id":2,"nav_item_type":1,"nav_item_type_id":"13","create_user_id":1,"update_user_id":1,"timestamp_create":1480536849,"timestamp_update":1481505780,"title":"Novedades y Ofertas","alias":"novedades-y-ofertas","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"Novedades y Ofertas","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 13),
(249, 1, 0, 1, 0, 1483476199, '{"tableName":"cms_nav_item","action":"update","row":13}', '{"id":13,"nav_id":12,"lang_id":2,"nav_item_type":1,"nav_item_type_id":13,"create_user_id":1,"update_user_id":1,"timestamp_create":1480536849,"timestamp_update":1483476199,"title":"Novedades y Ofertas","alias":"2017-01-03-21-43-novedades-y-ofertas","description":"OXIMED BOLIVIA - Alquiler y venta de botellones de ox\\u00edgeno medicinal a domicilio, man\\u00f3metros, bigoteras y mascarillas. Ox\\u00edgenos descartables Oxy Force. Alquiler de equipo ortop\\u00e9dico, sillas de ruedas, burrito especial, silla inodoro, bast\\u00f3n doble curvo, burritos andadores, bastones con asiento, camas hospitalarias","keywords":"Novedades y Ofertas","title_tag":"Oximed Bolivia"}', 'cms_nav_item', 13),
(250, 1, 0, 1, 0, 1483476200, '{"tableName":"cms_nav","action":"update","row":12}', '{"id":12,"nav_container_id":1,"parent_nav_id":0,"sort_index":12,"is_deleted":1,"is_hidden":0,"is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 12),
(251, 1, 1, 0, 0, 1483476266, '{"tableName":"cms_nav","action":"insert","row":13}', '{"parent_nav_id":"0","nav_container_id":"1","is_hidden":1,"is_offline":1,"is_draft":"0","sort_index":13,"id":13}', 'cms_nav', 13),
(252, 1, 1, 0, 0, 1483476266, '{"tableName":"cms_nav_item","action":"insert","row":14}', '{"lang_id":"2","title":"Novedades y Ofertas","alias":"novedades-y-ofertas","description":"","nav_item_type":1,"nav_item_type_id":14,"nav_id":13,"timestamp_create":1483476266,"timestamp_update":0,"create_user_id":1,"update_user_id":1,"id":14}', 'cms_nav_item', 14),
(253, 1, 0, 1, 0, 1483476272, '{"tableName":"cms_nav","action":"update","row":13}', '{"id":13,"nav_container_id":1,"parent_nav_id":0,"sort_index":13,"is_deleted":0,"is_hidden":1,"is_offline":"0","is_home":0,"is_draft":0}', 'cms_nav', 13),
(254, 1, 0, 1, 0, 1483476274, '{"tableName":"cms_nav","action":"update","row":13}', '{"id":13,"nav_container_id":1,"parent_nav_id":0,"sort_index":13,"is_deleted":0,"is_hidden":"0","is_offline":0,"is_home":0,"is_draft":0}', 'cms_nav', 13),
(255, 1, 1, 0, 0, 1483476288, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":49,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 49),
(256, 1, 0, 1, 0, 1483476359, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":49,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 49),
(257, 1, 1, 0, 0, 1483476742, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":50,"pageTitle":"Novedades y Ofertas","blockName":"Título"}', '[]', 'cms_nav_item_page_block_item', 50),
(258, 1, 0, 1, 0, 1483476800, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":50,"pageTitle":"Novedades y Ofertas","blockName":"Título"}', '[]', 'cms_nav_item_page_block_item', 50),
(259, 1, 0, 1, 0, 1483476808, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":50,"pageTitle":"Novedades y Ofertas","blockName":"Título"}', '[]', 'cms_nav_item_page_block_item', 50),
(260, 1, 0, 0, 1, 1483478222, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":49,"pageTitle":"Novedades y Ofertas","blockName":"Texto con imagen"}', '[]', 'cms_nav_item_page_block_item', 49),
(261, 1, 1, 0, 0, 1483478231, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":51,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 51),
(262, 1, 0, 1, 0, 1483478256, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":51,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 51),
(263, 1, 0, 1, 0, 1483479419, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":51,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 51),
(264, 1, 0, 1, 0, 1483480442, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":51,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 51),
(265, 1, 1, 0, 0, 1483480508, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":52,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 52),
(266, 1, 0, 1, 0, 1483480609, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":52,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 52),
(267, 1, 1, 0, 0, 1483480678, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":53,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 53),
(268, 1, 0, 1, 0, 1483480692, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":53,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 53),
(269, 1, 0, 1, 0, 1483480824, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":53,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 53),
(270, 1, 0, 0, 1, 1483480914, '{"tableName":"cms_nav_item_page_block_item","action":"delete","row":52,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 52),
(271, 1, 1, 0, 0, 1483481082, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":54,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 54),
(272, 1, 1, 0, 0, 1483481091, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":55,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 55),
(273, 1, 1, 0, 0, 1483481094, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":56,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 56),
(274, 1, 1, 0, 0, 1483481097, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":57,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 57),
(275, 1, 0, 1, 0, 1483481115, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":54,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 54),
(276, 1, 0, 1, 0, 1483481127, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":54,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 54),
(277, 1, 0, 1, 0, 1483481144, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":55,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 55),
(278, 1, 0, 1, 0, 1483481177, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":56,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 56),
(279, 1, 0, 1, 0, 1483481187, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":56,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 56),
(280, 1, 0, 1, 0, 1483481215, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":57,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 57),
(281, 1, 1, 0, 0, 1483481275, '{"tableName":"cms_nav_item_page_block_item","action":"insert","row":58,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 58),
(282, 1, 0, 1, 0, 1483481303, '{"tableName":"cms_nav_item_page_block_item","action":"update","row":58,"pageTitle":"Novedades y Ofertas","blockName":"Imagen"}', '[]', 'cms_nav_item_page_block_item', 58);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav`
--

CREATE TABLE `cms_nav` (
  `id` int(11) NOT NULL,
  `nav_container_id` int(11) NOT NULL DEFAULT '0',
  `parent_nav_id` int(11) NOT NULL DEFAULT '0',
  `sort_index` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0',
  `is_hidden` tinyint(1) DEFAULT '0',
  `is_offline` tinyint(1) DEFAULT '0',
  `is_home` tinyint(1) DEFAULT '0',
  `is_draft` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_nav`
--

INSERT INTO `cms_nav` (`id`, `nav_container_id`, `parent_nav_id`, `sort_index`, `is_deleted`, `is_hidden`, `is_offline`, `is_home`, `is_draft`) VALUES
(1, 1, 0, 1, 1, 0, 0, 0, 0),
(2, 1, 0, 2, 0, 0, 0, 0, 0),
(3, 1, 0, 3, 0, 0, 0, 1, 0),
(4, 1, 0, 4, 0, 0, 0, 0, 0),
(5, 1, 0, 5, 0, 0, 0, 0, 0),
(6, 1, 0, 6, 0, 0, 0, 0, 0),
(7, 1, 0, 7, 0, 0, 0, 0, 0),
(8, 1, 0, 8, 0, 0, 0, 0, 0),
(9, 1, 0, 9, 0, 0, 0, 0, 0),
(10, 1, 0, 10, 0, 0, 0, 0, 0),
(11, 1, 0, 11, 0, 0, 0, 0, 0),
(12, 1, 0, 12, 1, 0, 0, 0, 0),
(13, 1, 0, 13, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_container`
--

CREATE TABLE `cms_nav_container` (
  `id` int(11) NOT NULL,
  `name` varchar(180) NOT NULL,
  `alias` varchar(80) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_nav_container`
--

INSERT INTO `cms_nav_container` (`id`, `name`, `alias`, `is_deleted`) VALUES
(1, 'Inicio', 'inicio', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_item`
--

CREATE TABLE `cms_nav_item` (
  `id` int(11) NOT NULL,
  `nav_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `nav_item_type` int(11) NOT NULL,
  `nav_item_type_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `timestamp_create` int(11) DEFAULT NULL,
  `timestamp_update` int(11) DEFAULT NULL,
  `title` varchar(180) NOT NULL,
  `alias` varchar(80) NOT NULL,
  `description` text,
  `keywords` text,
  `title_tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_nav_item`
--

INSERT INTO `cms_nav_item` (`id`, `nav_id`, `lang_id`, `nav_item_type`, `nav_item_type_id`, `create_user_id`, `update_user_id`, `timestamp_create`, `timestamp_update`, `title`, `alias`, `description`, `keywords`, `title_tag`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1480446410, 1480446869, 'Homepage', '2016-11-29-20-14-homepage', NULL, NULL, NULL),
(2, 1, 2, 1, 2, 1, 1, 1480446838, 1480446870, 'Homepage', '2016-11-29-20-14-homepage', NULL, NULL, NULL),
(3, 2, 1, 1, 3, 1, 1, 1480446925, 0, 'Inicio', 'inicio', NULL, NULL, NULL),
(4, 3, 2, 1, 4, 1, 1, 1480447173, 1481505614, 'Oxígeno Medicinal Domiciliario', 'inicio', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'Oximed Bolivia, Oximed, Oxigeno medicinal, Equipos ortopedicos, Extintores, Oxigenoterapia a domicilio', 'Oximed Bolivia'),
(5, 4, 2, 1, 5, 1, 1, 1480510770, 1481505673, 'Nosotros', 'nosotros', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'Nosotros, Video presentación, Misión, Visión', 'Oximed Bolivia'),
(6, 5, 2, 1, 6, 1, 1, 1480510862, 1481505706, 'Contactos', 'contactos', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'Contáctenos', 'Oximed Bolivia'),
(7, 6, 2, 1, 7, 1, 1, 1480535950, 1481505723, 'Alquiler de equipos', 'alquiler-de-equipos', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'ALQUILER DE OXIGENOTERAPIA, OXIMED, terapias respiratorias domiciliarias, equipos de oxigeno', 'Oximed Bolivia'),
(8, 7, 2, 1, 8, 1, 1, 1480536149, 1481505734, 'Oxígeno medicinal', 'oxigeno-medicinal', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'Oxinoterapia, administración de oxígeno, Equipos de Oxigeno Medicinal', 'Oximed Bolivia'),
(9, 8, 2, 1, 9, 1, 1, 1480536300, 1481505743, 'Oxígeno industrial', 'oxigeno-industrial', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'OXIGENO INDUSTRIAL', 'Oximed Bolivia'),
(10, 9, 2, 1, 10, 1, 1, 1480536421, 1481505753, 'Accesorios', 'accesorios', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'ACCESORIOS', 'Oximed Bolivia'),
(11, 10, 2, 1, 11, 1, 1, 1480536524, 1481505762, 'Equipos ortopédicos', 'equipos-ortopedicos', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'EQUIPOS ORTOPÉDICOS', 'Oximed Bolivia'),
(12, 11, 2, 1, 12, 1, 1, 1480536677, 1481505771, 'Extintores', 'extintores', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'EXTINTORES', 'Oximed Bolivia'),
(13, 12, 2, 1, 13, 1, 1, 1480536849, 1483476199, 'Novedades y Ofertas', '2017-01-03-21-43-novedades-y-ofertas', 'OXIMED BOLIVIA - Alquiler y venta de botellones de oxígeno medicinal a domicilio, manómetros, bigoteras y mascarillas. Oxígenos descartables Oxy Force. Alquiler de equipo ortopédico, sillas de ruedas, burrito especial, silla inodoro, bastón doble curvo, burritos andadores, bastones con asiento, camas hospitalarias', 'Novedades y Ofertas', 'Oximed Bolivia'),
(14, 13, 2, 1, 14, 1, 1, 1483476266, 1483481303, 'Novedades y Ofertas', 'novedades-y-ofertas', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_item_module`
--

CREATE TABLE `cms_nav_item_module` (
  `id` int(11) NOT NULL,
  `module_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_item_page`
--

CREATE TABLE `cms_nav_item_page` (
  `id` int(11) NOT NULL,
  `layout_id` int(11) DEFAULT NULL,
  `nav_item_id` int(11) NOT NULL,
  `timestamp_create` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `version_alias` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_nav_item_page`
--

INSERT INTO `cms_nav_item_page` (`id`, `layout_id`, `nav_item_id`, `timestamp_create`, `create_user_id`, `version_alias`) VALUES
(1, 1, 1, 1480446410, 1, 'Initial'),
(2, 1, 2, 1480446410, 1, 'Versión inicial'),
(3, 1, 3, 1480446924, 1, 'Initial'),
(4, 1, 4, 1480447173, 1, 'Initial'),
(5, 1, 5, 1480510770, 1, 'Initial'),
(6, 1, 6, 1480510862, 1, 'Initial'),
(7, 1, 7, 1480535949, 1, 'Initial'),
(8, 1, 8, 1480536149, 1, 'Initial'),
(9, 1, 9, 1480536300, 1, 'Initial'),
(10, 1, 10, 1480536420, 1, 'Initial'),
(11, 1, 11, 1480536524, 1, 'Initial'),
(12, 1, 12, 1480536676, 1, 'Initial'),
(13, 1, 13, 1480536849, 1, 'Initial'),
(14, 2, 14, 1483476265, 1, 'Initial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_item_page_block_item`
--

CREATE TABLE `cms_nav_item_page_block_item` (
  `id` int(11) NOT NULL,
  `block_id` int(11) DEFAULT NULL,
  `placeholder_var` varchar(80) NOT NULL,
  `nav_item_page_id` int(11) DEFAULT NULL,
  `prev_id` int(11) DEFAULT NULL,
  `json_config_values` text,
  `json_config_cfg_values` text,
  `is_dirty` tinyint(1) DEFAULT '0',
  `create_user_id` int(11) DEFAULT '0',
  `update_user_id` int(11) DEFAULT '0',
  `timestamp_create` int(11) DEFAULT '0',
  `timestamp_update` int(11) DEFAULT '0',
  `sort_index` int(11) DEFAULT '0',
  `is_hidden` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_nav_item_page_block_item`
--

INSERT INTO `cms_nav_item_page_block_item` (`id`, `block_id`, `placeholder_var`, `nav_item_page_id`, `prev_id`, `json_config_values`, `json_config_cfg_values`, `is_dirty`, `create_user_id`, `update_user_id`, `timestamp_create`, `timestamp_update`, `sort_index`, `is_hidden`) VALUES
(12, 5, 'content', 5, 0, '{"html":"<div class=\\"wrap\\">\\n    <article id=\\"nosotros\\">\\n        <h2 id=\\"nosotros\\">Nosotros<\\/h2>\\n\\n        <div class=\\"row\\">\\n            <div class=\\"col-sm-6\\">\\n              <h3>Misi\\u00f3n<\\/h3>\\n                <p style=\\"margin-left:20px; margin-right:20px;\\">Nuestra misi\\u00f3n es ser una empresa exitosa que mantenga el liderazgo en la soluci\\u00f3n de los problemas respiratorios.<\\/p>\\n              <h3>Visi\\u00f3n<\\/h3>\\n                <p style=\\"margin-left:20px; margin-right:20px;\\">Poder lograr un nivel de satisfacci\\u00f3n que va m\\u00e1s all\\u00e1 de las expectativas de nuestro clientes que sufren de alg\\u00fan problema respiratorio, mediante la continua innovaci\\u00f3n de tecnolog\\u00eda de \\u00faltima generaci\\u00f3n.<\\/p>\\n            <\\/div>\\n            <div class=\\"col-sm-6\\">\\n              <h3>Video presentaci\\u00f3n<\\/h3>\\n              <div class=\\"mainarticle nopad animateblock\\" id=\\"mapa\\">\\n                <div class=\\"freewrapper\\">\\n                  <div class=\\"row\\">\\n                      <div class=\\"col100\\">\\n                    <iframe  width=\\"360\\" height=\\"310\\" src=\\"https:\\/\\/www.youtube.com\\/embed\\/RbJJll0Cng8?rel=0&autoplay=1\\" frameborder=\\"0\\" allowfullscreen\\"><\\/iframe>\\n                      <\\/div>\\n                  <\\/div>\\n                <\\/div>  <!--  <img class=\\"fullw\\" src=\\"img\\/inicio-img.jpg\\" alt=\\"imagen de inicio\\"> -->\\n              <\\/div>\\n          <\\/div>\\n\\n      <\\/div>\\n    <\\/article>\\n<\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480510816, 1481322701, 0, 0),
(13, 5, 'content', 4, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <div style=\\"overflow:hidden; auto; padding-top:15px; max-width:100%;\\">\\n      <section class=\\"slider-content\\" style=\\"height: auto;\\">\\n        <ul class=\\"mySlideshow\\">\\n          <li class=\\"primero\\">\\n                <a class=\\" texto-slider-der\\" href=\\"equipos-ortopedicos\\">EQUIPOS ORTOPEDICOS<\\/a>\\n            <img class=\\"img-responsive\\" style=\\"position:absolute; right:2%;bottom:5%\\" src=\\"images\\/slides\\/icon-slide.png\\" alt=\\"\\" \\/>\\n            <img class=\\"img-normal\\" style=\\"float:left;\\" src=\\"images\\/slides\\/slider-01.jpg\\"\\/>\\n            <img class=\\"img-res\\" style=\\"float:left;\\" src=\\"images\\/slides\\/slider-01-responsive.jpg\\"\\/>\\n          <\\/li>\\n          <li class=\\"segundo\\">\\n                <a class=\\" texto-slider-izq\\" href=\\"oxigeno-medicinal\\">EQUIPOS DE OXIGENO MEDICINAL<\\/span><\\/a>\\n            <img class=\\"img-responsive\\" style=\\"position:absolute; left:2%;bottom:5%\\" src=\\"images\\/slides\\/icon-slide.png\\" alt=\\"\\" \\/>\\n            <img class=\\"img-normal\\" style=\\"float:right;\\" src=\\"images\\/slides\\/slider-02.jpg\\"\\/>\\n            <img class=\\"img-res\\" style=\\"float:right;\\" src=\\"images\\/slides\\/slider-02-responsive.jpg\\"\\/>\\n          <\\/li>\\n          <li class=\\"tercero\\">\\n                <a class=\\" texto-slider-der\\" href=\\"extintores\\"> EXTINTORES<\\/a>\\n            <img class=\\"img-responsive\\" style=\\"position:absolute; right:2%;bottom:5%\\" src=\\"images\\/slides\\/icon-slide.png\\" alt=\\"\\" \\/>\\n            <img class=\\"img-normal\\" style=\\"float:left;\\" src=\\"images\\/slides\\/slider-03.jpg\\"\\/>\\n            <img class=\\"img-res\\" style=\\"float:left;\\" src=\\"images\\/slides\\/slider-03-responsive.jpg\\"\\/>\\n          <\\/li>\\n          <li class=\\"cuarto\\">\\n                <a class=\\" texto-slider-izq\\" href=\\"accesorios\\">ACCESORIOS<\\/a>\\n            <img class=\\"img-responsive\\" style=\\"position:absolute; left:2%;bottom:5%\\" src=\\"images\\/slides\\/icon-slide.png\\" alt=\\"\\" \\/>\\n            <img class=\\"img-normal\\" style=\\"float:right;\\" src=\\"images\\/slides\\/slider-04.jpg\\"\\/>\\n            <img class=\\"img-res\\" style=\\"float:right;\\" src=\\"images\\/slides\\/slider-04-responsive.jpg\\"\\/>\\n          <\\/li>\\n        <\\/ul>\\n      <\\/section>\\n    <\\/div>\\n<\\/div>\\n<div class=\\"wrap\\">\\n        <article>\\n            <h2>Bienvenido a nuestro sitio web<\\/h2>\\n            <div class=\\"freewrapper inicio\\">\\n                <div class=\\"wpad\\">\\n                    <p>Bienvenidos a la p\\u00e1gina Web de Oximed Bolivia, empresa especializada en servicios de Oxigenoterapia a domicilio las 24 horas y los 365 d\\u00edas del a\\u00f1o.<\\/p>\\n                    <p>Somos una empresa, que entrega soluciones integrales en terapias respiratorias domiciliarias, poniendo al alcance de sus clientes, equipos de oxigeno que permitan manejar con calidad, seguridad y confiabilidad en las terapias requeridas.\\n                      Para ello, OXIMED trabaja las 24 horas y los 365 d\\u00edas del a\\u00f1o, siendo una empresa de rubro con mejor cobertura en la Ciudad del Alto y de La Paz.\\n                      El Personal a cargo est\\u00e1 capacitado para entregar un servicio de calidad y confiable y cuenta con un sistema de comunicaci\\u00f3n que le permite responder a las urgencias requeridas.<\\/p>\\n                <\\/div>\\n            <\\/div>\\n        <\\/article>\\n    <\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480534180, 1480709793, 0, 0),
(15, 5, 'content', 6, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <article id=\\"contacto\\">\\n        <div class=\\"freewrapper row\\" id=\\"contactenos\\">\\n            <div class=\\"col-sm-4\\" style=\\"padding-right: 0px;padding-left: 0px;\\">\\n              <h2>Cont\\u00e1ctenos<\\/h2>\\n                <div id=\\"contactos\\" class=\\"mainarticle nopad animateblock\\">\\n                    <form method=\\"post\\" id=\\"theForm\\" class=\\"second\\" action=\\"<?= $this->publicHtml; ?>mail.php\\" role=\\"form\\">\\n                      <div class=\\"form_row\\">\\n                        <div class=\\"input\\">\\n                          <input type=\\"text\\" id=\\"nombre\\" name=\\"nombre\\" tabindex=\\"1\\" required>\\n                          <label for=\\"nombre\\">Nombre:<\\/label>\\n                        <\\/div>\\n                      <\\/div><!-- @end row #1 -->\\n\\n                      <div class=\\"form_row\\">\\n                        <div class=\\"input\\">\\n                          <input type=\\"text\\" id=\\"telefono\\" name=\\"telefono\\" tabindex=\\"2\\" required>\\n                          <label for=\\"telefono\\">Tel\\u00e9fono:<\\/label>\\n                        <\\/div>\\n                      <\\/div><!-- @end row #2 -->\\n\\n                      <div class=\\"form_row\\">\\n                        <div class=\\"input\\">\\n                          <input type=\\"text\\" id=\\"movil\\" name=\\"movil\\" tabindex=\\"3\\" required>\\n                          <label for=\\"movil\\">Celular:<\\/label>\\n                        <\\/div>\\n                      <\\/div><!-- @end row #2 -->\\n\\n                      <div class=\\"form_row\\">\\n                        <div class=\\"input\\">\\n                          <input type=\\"text\\" id=\\"direccion\\" name=\\"direccion\\" tabindex=\\"4\\" required>\\n                          <label for=\\"direccion\\">Direcci\\u00f3n:<\\/label>\\n                        <\\/div>\\n                      <\\/div><!-- @end row #2 -->\\n\\n                      <div class=\\"form_row\\">\\n                        <div class=\\"input\\">\\n                          <input type=\\"text\\" id=\\"ciudad\\" name=\\"ciudad\\" tabindex=\\"5\\" required>\\n                          <label for=\\"ciudad\\">Ciudad:<\\/label>\\n                        <\\/div>\\n                      <\\/div><!-- @end row #4 -->\\n                      <div class=\\"form_row\\">\\n                        <div class=\\"input\\">\\n                          <input type=\\"email\\" id=\\"email\\" name=\\"email\\" tabindex=\\"6\\" required>\\n                          <label for=\\"email\\">Su e-mail:<\\/label>\\n                        <\\/div>\\n                      <\\/div><!-- @end row #2 -->\\n\\n                      <div class=\\"form_row mensaje\\">\\n                        <div class=\\"input\\">\\n                          <textarea id=\\"mensaje\\" cols=\\"55\\" rows=\\"7\\" name=\\"mensaje\\" tabindex=\\"7\\" required><\\/textarea>\\n                          <label for=\\"mensaje\\">Mensaje:<\\/label>\\n                        <\\/div>\\n                      <\\/div><!-- @end row #5 -->\\n                      <div class=\\"form_row botones\\">\\n                        <input class=\\"submitbtn\\" type=\\"submit\\" tabindex=\\"8\\" value=\\"Enviar\\">\\n                        <input class=\\"deletebtn\\" type=\\"reset\\" tabindex=\\"9\\" value=\\"Borrar\\">\\n                      <\\/div>\\n                    <\\/form>\\n                    <div class=\\"clear\\"><\\/div>\\n                    <div id=\\"statusMessage\\"><\\/div>\\n                <\\/div><!-- fin de contactos -->\\n            <\\/div><!-- fin de colfor -->\\n            <div class=\\"col-sm-8\\" style=\\"padding-right: 0px;padding-left: 0px;\\">\\n              <h2>Ub\\u00edquenos<\\/h2>\\n                <div class=\\"mainarticle nopad animateblock\\" id=\\"mapa\\">\\n                    <div class=\\"freewrapper\\">\\n                        <div class=\\"row\\">\\n                            <div class=\\"col100\\">\\n                                <iframe src=\\"https:\\/\\/www.google.com\\/maps\\/d\\/embed?mid=1sFYDFI24iLZvycH971nEVYQOBRg\\" width=\\"640\\" height=\\"480\\"><\\/iframe>\\n                            <\\/div>\\n                        <\\/div>\\n                    <\\/div>\\n                <\\/div>\\n            <\\/div>\\n        <\\/div><!-- fin de freewrapper -->\\n    <\\/article>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480535514, 1480535811, 0, 0),
(16, 5, 'content', 7, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <article>\\n        <h2>ALQUILER DE OXIGENOTERAPIA<\\/h2>\\n        <div class=\\"freewrapper\\">\\n          <p style=\\"margin-left:50px; margin-right:50px;margin-top:20px;\\">Somos una empresa, que entrega soluciones integrales en terapias respiratorias domiciliarias, equipos de oxigeno de diferentes tama\\u00f1os como ser:<\\/p>\\n            <div class=\\"col-sm-4 col-xs-12\\">\\n              <p style=\\"text-align:center\\"><i class=\\"fa fa-angle-double-right\\" aria-hidden=\\"true\\"><\\/i> 6m3 (hospitalario)<\\/p>\\n              <p style=\\"text-align:center\\"><i class=\\"fa fa-angle-double-right\\" aria-hidden=\\"true\\"><\\/i> 4m3 (mediano)<\\/p>\\n            <\\/div>\\n            <div class=\\"col-sm-4 col-xs-12\\">\\n              <p style=\\"text-align:center\\"><i class=\\"fa fa-angle-double-right\\" aria-hidden=\\"true\\"><\\/i> 1700 L. (mediano)<\\/p>\\n              <p style=\\"text-align:center\\"><i class=\\"fa fa-angle-double-right\\" aria-hidden=\\"true\\"><\\/i> 682 L. (Port\\u00e1til)<\\/p>\\n            <\\/div>\\n            <div class=\\"col-sm-4 col-xs-12\\">\\n              <p style=\\"text-align:center\\"><i class=\\"fa fa-angle-double-right\\" aria-hidden=\\"true\\"><\\/i> 415 L. (Port\\u00e1til)<\\/p>\\n              <p style=\\"text-align:center\\"><i class=\\"fa fa-angle-double-right\\" aria-hidden=\\"true\\"><\\/i> 170 L. (Port\\u00e1til)<\\/p>\\n            <\\/div>\\n          <p style=\\"margin-left:50px; margin-right:50px;margin-top:90px; text-align:center;\\">Para ello, OXIMED trabaja las 24 horas y los 365 d\\u00edas del a\\u00f1o.<\\/p><br>\\n          <div class=\\"freewrapper\\">\\n          <ul class=\\"lb-album\\" style=\\"margin-top: -20px;\\">\\n          \\t<li id=\\"ban-alqui\\">\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-9.jpg\\" alt=\\"image09\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-9.jpg\\" alt=\\"image09\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li id=\\"ban-alqui\\">\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-10.jpg\\" alt=\\"image10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-10.jpg\\" alt=\\"image02\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t<\\/ul>\\n        <\\/div>\\n      <\\/div>\\n    <\\/article>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480535964, 1480536015, 0, 0),
(17, 5, 'content', 8, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <article>\\n        <h2>Oxinoterapia<\\/h2>\\n        <br>\\n        <div class=\\"freewrapper\\">\\n          <div class=\\"col-sm-3 col-md-4 col-lg-3\\">\\n            <img class=\\"img-responsive center-block\\" src=\\"images\\/oxinoterapia2.jpg\\" alt=\\"image02\\">\\n          <\\/div>\\n          <div class=\\"col-sm-6 col-md-4 col-lg-6\\"><br>\\n            <p style=\\"font-size:23px;\\">La oxigenoterapia es una medida terap\\u00e9utica que consiste en la administraci\\u00f3n de ox\\u00edgeno a pacientes con enfermedades pulmonares o de otra causa, que requieren para su tratamiento evitando as\\u00ed hospitalizaciones innecesarias.<\\/p>\\n          <\\/div>\\n          <div class=\\"col-sm-3 col-md-4 col-lg-3\\">\\n            <img class=\\"img-responsive center-block\\" src=\\"images\\/oxinoterapia1.jpg\\" alt=\\"image01\\">\\n          <\\/div>\\n        <\\/div>\\n        <br>\\n        <h2>Equipos de Oxigeno Medicinal<\\/h2>\\n        <div class=\\"freewrapper\\">\\n          <ul class=\\"lb-album\\">\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-1.jpg\\" alt=\\"image01\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/oxi-1.jpg\\" alt=\\"image01\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-2.jpg\\" alt=\\"image02\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/oxi-2.jpg\\" alt=\\"image02\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-3.jpg\\" alt=\\"image03\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/oxi-3.jpg\\" alt=\\"image03\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-4.jpg\\" alt=\\"image04\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/oxi-4.jpg\\" alt=\\"image04\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-5.jpg\\" alt=\\"image05\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/oxi-5.jpg\\" alt=\\"image05\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-6.jpg\\" alt=\\"image06\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/oxi-6.jpg\\" alt=\\"image06\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-1.jpg\\" alt=\\"image07\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-1.jpg\\" alt=\\"image07\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n\\n            <!--\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-7.jpg\\" alt=\\"image07\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/oxi-7.jpg\\" alt=\\"image07\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/oxi-8.jpg\\" alt=\\"image08\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/oxi-8.jpg\\" alt=\\"image08\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n          -->\\n  \\t\\t\\t\\t<\\/ul>\\n        <\\/div>\\n    <\\/article>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480536162, 1480709938, 0, 0),
(18, 5, 'content', 9, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <article>\\n        <h2>OXIGENO INDUSTRIAL<\\/h2>\\n        <div class=\\"freewrapper\\">\\n          <ul class=\\"lb-album\\">\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-3.jpg\\" alt=\\"image03\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-3.jpg\\" alt=\\"image03\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-4.jpg\\" alt=\\"image04\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-4.jpg\\" alt=\\"image04\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <li>\\n              <a href=\\"#image-5\\">\\n                <img src=\\"img\\/thumbs\\/alqui-2.jpg\\" alt=\\"image05\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-5\\">\\n                <img src=\\"img\\/full\\/alqui-2.jpg\\" alt=\\"image02\\" \\/>\\n                <div>\\n                  <a href=\\"#image-4\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-3\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n            <!--\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-3.jpg\\" alt=\\"image03\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-3.jpg\\" alt=\\"image03\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-4.jpg\\" alt=\\"image04\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-4.jpg\\" alt=\\"image04\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-5.jpg\\" alt=\\"image05\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-5.jpg\\" alt=\\"image05\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-6.jpg\\" alt=\\"image06\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-6.jpg\\" alt=\\"image06\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-7.jpg\\" alt=\\"image07\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-7.jpg\\" alt=\\"image07\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-8.jpg\\" alt=\\"image08\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-8.jpg\\" alt=\\"image08\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-9.jpg\\" alt=\\"image09\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-9.jpg\\" alt=\\"image09\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<h3> href=\\"#image-8\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/reg-10.jpg\\" alt=\\"image10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/reg-10.jpg\\" alt=\\"image10\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n          -->\\n  \\t\\t\\t\\t<\\/ul>\\n        <\\/div>\\n    <\\/article>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480536311, 1480536359, 0, 0);
INSERT INTO `cms_nav_item_page_block_item` (`id`, `block_id`, `placeholder_var`, `nav_item_page_id`, `prev_id`, `json_config_values`, `json_config_cfg_values`, `is_dirty`, `create_user_id`, `update_user_id`, `timestamp_create`, `timestamp_update`, `sort_index`, `is_hidden`) VALUES
(19, 5, 'content', 10, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <article>\\n        <h2>ACCESORIOS<\\/h2>\\n        <div class=\\"freewrapper\\">\\n          <ul class=\\"lb-album row\\">\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-1.jpg\\" alt=\\"image01\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-1.jpg\\" alt=\\"image01\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-2.jpg\\" alt=\\"image02\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-2.jpg\\" alt=\\"image02\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-3.jpg\\" alt=\\"image03\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-3\\">\\n\\n\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-3.jpg\\" alt=\\"image03\\" \\/>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n\\n                <div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-4.jpg\\" alt=\\"image04\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-4.jpg\\" alt=\\"image04\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-5.jpg\\" alt=\\"image05\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-5.jpg\\" alt=\\"image05\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-6.jpg\\" alt=\\"image06\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-6.jpg\\" alt=\\"image06\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-7.jpg\\" alt=\\"image07\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-7.jpg\\" alt=\\"image07\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <li>\\n              <a href=\\"#image-8\\">\\n                <img src=\\"img\\/thumbs\\/reg-1.jpg\\" alt=\\"image08\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-8\\">\\n                <img src=\\"img\\/full\\/reg-1.jpg\\" alt=\\"image08\\" \\/>\\n                <div>\\n                  <a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-9\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n            <li>\\n              <a href=\\"#image-9\\">\\n                <img src=\\"img\\/thumbs\\/reg-2.jpg\\" alt=\\"image09\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-9\\">\\n                <img src=\\"img\\/full\\/reg-2.jpg\\" alt=\\"image09\\" \\/>\\n                <div>\\n                  <a href=\\"#image-8\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n<!--\\n          \\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-8.jpg\\" alt=\\"image08\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-8.jpg\\" alt=\\"image08\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-1.jpg\\" alt=\\"image09\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-1.jpg\\" alt=\\"image09\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n\\t\\t\\t\\t\\t\\t      <a href=\\"#image-8\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a class=\\"col-xs-12 col-sm-6 col-md-4 col-lg-3\\" href=\\"#image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive\\" src=\\"img\\/thumbs\\/acce-1.jpg\\" alt=\\"image10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay col-xs-12\\" id=\\"image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img class=\\"img-responsive center-block\\" src=\\"img\\/full\\/acce-1.jpg\\" alt=\\"image10\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"row\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n          -->\\n  \\t\\t\\t\\t<\\/ul>\\n        <\\/div>\\n    <\\/article>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480536429, 1480536461, 0, 0),
(20, 5, 'content', 11, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <article>\\n        <h2>EQUIPOS ORTOP\\u00c9DICOS<\\/h2>\\n        <div class=\\"freewrapper\\">\\n          <ul class=\\"lb-album\\">\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-1.jpg\\" alt=\\"image01\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-1.jpg\\" alt=\\"image01\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div class=\\"sig-ant\\">\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-16\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-2.jpg\\" alt=\\"image02\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-2.jpg\\" alt=\\"image02\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-3.jpg\\" alt=\\"image03\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-3\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-3.jpg\\" alt=\\"image03\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-4.jpg\\" alt=\\"image04\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-4\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-4.jpg\\" alt=\\"image04\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-3\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-5.jpg\\" alt=\\"image05\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-5.jpg\\" alt=\\"image05\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t<!--\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-1.jpg\\" alt=\\"image06\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-3.jpg\\" alt=\\"image06\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li> -->\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-7.jpg\\" alt=\\"image07\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-7.jpg\\" alt=\\"image07\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t<!--\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-1.jpg\\" alt=\\"image08\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-3.jpg\\" alt=\\"image08\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li> -->\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-9.jpg\\" alt=\\"image09\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-9\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-9.jpg\\" alt=\\"image09\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-10.jpg\\" alt=\\"image10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-10\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-10.jpg\\" alt=\\"image10\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-11\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-11\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/ortopedico-11.jpg\\" alt=\\"image11\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-11\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/ortopedico-11.jpg\\" alt=\\"image11\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-10\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <li>\\n              <a href=\\"#image-12\\">\\n                <img src=\\"img\\/thumbs\\/ortopedico-12.jpg\\" alt=\\"image12\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-12\\">\\n                <img src=\\"img\\/full\\/ortopedico-12.jpg\\" alt=\\"image12\\" \\/>\\n                <div>\\n                  <a href=\\"#image-11\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-13\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n            <li>\\n              <a href=\\"#image-13\\">\\n                <img src=\\"img\\/thumbs\\/ortopedico-13.jpg\\" alt=\\"image13\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-13\\">\\n                <img src=\\"img\\/full\\/ortopedico-13.jpg\\" alt=\\"image13\\" \\/>\\n                <div>\\n                  <a href=\\"#image-12\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-14\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n            <li>\\n              <a href=\\"#image-14\\">\\n                <img src=\\"img\\/thumbs\\/ortopedico-14.jpg\\" alt=\\"image14\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-14\\">\\n                <img src=\\"img\\/full\\/ortopedico-14.jpg\\" alt=\\"image14\\" \\/>\\n                <div>\\n                  <a href=\\"#image-13\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-15\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n            <li>\\n              <a href=\\"#image-15\\">\\n                <img src=\\"img\\/thumbs\\/ortopedico-15.jpg\\" alt=\\"image15\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-15\\">\\n                <img src=\\"img\\/full\\/ortopedico-15.jpg\\" alt=\\"image15\\" \\/>\\n                <div>\\n                  <a href=\\"#image-14\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-16\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n            <li>\\n              <a href=\\"#image-16\\">\\n                <img src=\\"img\\/thumbs\\/ortopedico-16.jpg\\" alt=\\"image16\\">\\n                <span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n              <\\/a>\\n              <div class=\\"lb-overlay\\" id=\\"image-16\\">\\n                <img src=\\"img\\/full\\/ortopedico-16.jpg\\" alt=\\"image10\\" \\/>\\n                <div>\\n                  <a href=\\"#image-15\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n                  <a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n                <\\/div>\\n                <a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n              <\\/div>\\n            <\\/li>\\n  \\t\\t\\t\\t<\\/ul>\\n        <\\/div>\\n    <\\/article>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480536561, 1480543123, 0, 0),
(21, 5, 'content', 12, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n<div class=\\"wrap\\">\\n    <article>\\n        <h2>EXTINTORES<\\/h2>\\n        <div class=\\"freewrapper\\">\\n          <ul class=\\"lb-album\\">\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/extin-1.jpg\\" alt=\\"image01\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-1\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/extin-1.jpg\\" alt=\\"image01\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/extin-2.jpg\\" alt=\\"image02\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-2\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/extin-2.jpg\\" alt=\\"image02\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-5.jpg\\" alt=\\"image05\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-5.jpg\\" alt=\\"image05\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-2\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-6.jpg\\" alt=\\"image06\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-6.jpg\\" alt=\\"image06\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-7.jpg\\" alt=\\"image07\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-7.jpg\\" alt=\\"image07\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/alqui-8.jpg\\" alt=\\"image08\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/alqui-8.jpg\\" alt=\\"image08\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-1\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n            <!--\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/extin-1.jpg\\" alt=\\"image05\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-5\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/extin-1.jpg\\" alt=\\"image05\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-4\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/extin-1.jpg\\" alt=\\"image06\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-6\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/extin-1.jpg\\" alt=\\"image06\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-5\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/extin-1.jpg\\" alt=\\"image07\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-7\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/extin-1.jpg\\" alt=\\"image07\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-6\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n  \\t\\t\\t\\t\\t<li>\\n  \\t\\t\\t\\t\\t\\t<a href=\\"#image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/thumbs\\/extin-1.jpg\\" alt=\\"image08\\">\\n  \\t\\t\\t\\t\\t\\t\\t<span>Ver detalles <i class=\\"fa fa-search-plus\\" aria-hidden=\\"true\\"><\\/i><\\/span>\\n  \\t\\t\\t\\t\\t\\t<\\/a>\\n  \\t\\t\\t\\t\\t\\t<div class=\\"lb-overlay\\" id=\\"image-8\\">\\n  \\t\\t\\t\\t\\t\\t\\t<img src=\\"img\\/full\\/extin-1.jpg\\" alt=\\"image08\\" \\/>\\n  \\t\\t\\t\\t\\t\\t\\t<div>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-7\\" class=\\"col-xs-6 anterior-prev\\"><i class=\\"fa fa-arrow-circle-left\\" aria-hidden=\\"true\\"><\\/i> Anterior<\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t\\t<a href=\\"#image-9\\" class=\\"col-xs-6 siguiente-next\\">Siguiente <i class=\\"fa fa-arrow-circle-right\\" aria-hidden=\\"true\\"><\\/i><\\/a>\\n  \\t\\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t\\t\\t<a href=\\"#page\\" class=\\"lb-close\\"><i class=\\"fa fa-times-circle-o\\" aria-hidden=\\"true\\"><\\/i> Cerrar<\\/a>\\n  \\t\\t\\t\\t\\t\\t<\\/div>\\n  \\t\\t\\t\\t\\t<\\/li>\\n          -->\\n  \\t\\t\\t\\t<\\/ul>\\n        <\\/div>\\n    <\\/article>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480536687, 1480536743, 0, 0),
(22, 5, 'content', 13, 0, '{"html":"<div class=\\"clear\\"><\\/div>\\n    <div class=\\"wrap\\">\\n    <article>\\n      <h2>Novedades y Ofertas<\\/h2>\\n<br>"}', '{"__e":"__o"}', 1, 1, 1, 1480536878, 1480602427, 0, 0),
(23, 5, 'content', 13, 0, '{"html":"<\\/article>\\n    <\\/div>\\n<\\/div>\\n<div class=\\"clear\\"><\\/div>"}', '{"__e":"__o"}', 1, 1, 1, 1480536939, 1480557699, 3, 0),
(25, 8, 'content', 13, 0, '{"width":6}', '{"rowDivClass":"row","leftColumnClasses":"","rightColumnClasses":""}', 1, 1, 1, 1480542658, 1480710169, 1, 0),
(35, 8, 'left', 13, 25, '{"width":6}', '{"leftColumnClasses":"col-xs-12 col-sm-6","rightColumnClasses":"col-xs-12 col-sm-6"}', 1, 1, 1, 1480610666, 1480630114, 0, 0),
(36, 8, 'right', 13, 25, '{"width":6}', '{"leftColumnClasses":"col-sm-6 col-xs-12","rightColumnClasses":"col-sm-6 col-xs-12"}', 1, 1, 1, 1480610678, 1480610938, 0, 0),
(39, 6, 'right', 13, 35, '{"textType":0,"imageId":7,"caption":"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est\\u00e1ndar de las industrias desde el a\\u00f1o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)"}', '{"__e":"__o"}', 1, 1, 1, 1480611028, 1480630164, 0, 0),
(40, 6, 'left', 13, 36, '{"textType":0,"imageId":7,"caption":"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est\\u00e1ndar de las industrias desde el a\\u00f1o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)"}', '{"__e":"__o"}', 1, 1, 1, 1480611032, 1480630161, 0, 0),
(41, 6, 'right', 13, 36, '{"textType":0,"imageId":7,"caption":"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est\\u00e1ndar de las industrias desde el a\\u00f1o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)"}', '{"__e":"__o"}', 1, 1, 1, 1480611036, 1480630159, 0, 0),
(42, 8, 'content', 13, 0, '{"width":6}', '{"rowDivClass":"row","leftColumnClasses":"","rightColumnClasses":""}', 1, 1, 1, 1480542658, 1480693920, 2, 0),
(43, 8, 'left', 13, 42, '{"width":6}', '{"leftColumnClasses":"col-xs-12 col-sm-6","rightColumnClasses":"col-xs-12 col-sm-6"}', 1, 1, 1, 1480631011, 1480631011, 0, 0),
(44, 6, 'left', 13, 43, '{"textType":0,"imageId":7,"caption":"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est\\u00e1ndar de las industrias desde el a\\u00f1o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)."}', '{"__e":"__o"}', 1, 1, 1, 1480631011, 1480631011, 0, 0),
(45, 6, 'right', 13, 43, '{"textType":0,"imageId":7,"caption":"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est\\u00e1ndar de las industrias desde el a\\u00f1o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)"}', '{"__e":"__o"}', 1, 1, 1, 1480631011, 1480631011, 0, 0),
(46, 8, 'right', 13, 42, '{"width":6}', '{"leftColumnClasses":"col-sm-6 col-xs-12","rightColumnClasses":"col-sm-6 col-xs-12"}', 1, 1, 1, 1480631011, 1480631011, 0, 0),
(47, 6, 'left', 13, 46, '{"textType":0,"imageId":7,"caption":"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est\\u00e1ndar de las industrias desde el a\\u00f1o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)"}', '{"__e":"__o"}', 1, 1, 1, 1480631011, 1480631011, 0, 0),
(48, 6, 'left', 13, 35, '{"textType":0,"imageId":7,"caption":"Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno est\\u00e1ndar de las industrias desde el a\\u00f1o 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)"}', '{"__e":"__o"}', 1, 1, 1, 1480631011, 1480637933, 0, 0),
(50, 18, 'TITULO', 14, 0, '{"headingType":"h2","content":"NOVEDADES Y OFERTAS"}', '{"__e":"__o"}', 1, 1, 1, 1483476742, 1483476808, 0, 0),
(51, 6, 'columna1', 14, 0, '{"textType":0,"imageId":13,"caption":"Esto es una prueba de una novedad\\nsados a todooasn\\nsdasdsdf\\nsdf\\n\\ndg\\ndfg"}', '{"externalLink":"imagen1","cssClass":""}', 1, 1, 1, 1483478230, 1483480441, 0, 0),
(53, 6, 'columna1', 14, 0, '{"textType":0,"imageId":16,"caption":"Prueba 2"}', '{"externalLink":"dddff"}', 1, 1, 1, 1483480677, 1483480824, 1, 0),
(54, 6, 'insertarimagen', 14, 0, '{"textType":0,"imageId":13,"caption":"Prueba1"}', '{"externalLink":"prueba1"}', 1, 1, 1, 1483481082, 1483481127, 0, 0),
(55, 6, 'insertarimagen', 14, 0, '{"textType":0,"imageId":16,"caption":"Prueba 2"}', '{"externalLink":"prueba2"}', 1, 1, 1, 1483481091, 1483481143, 1, 0),
(56, 6, 'insertarimagen', 14, 0, '{"textType":0,"imageId":19,"caption":"prueba3"}', '{"externalLink":"prueba3"}', 1, 1, 1, 1483481094, 1483481186, 2, 0),
(57, 6, 'insertarimagen', 14, 0, '{"textType":0,"imageId":22,"caption":"prueba 4"}', '{"externalLink":"prueba4"}', 1, 1, 1, 1483481097, 1483481214, 3, 0),
(58, 6, 'insertarimagen', 14, 0, '{"textType":0,"imageId":25,"caption":"prueba 5"}', '{"externalLink":"prueba5"}', 1, 1, 1, 1483481275, 1483481303, 4, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_item_redirect`
--

CREATE TABLE `cms_nav_item_redirect` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_permission`
--

CREATE TABLE `cms_nav_permission` (
  `group_id` int(11) NOT NULL,
  `nav_id` int(11) NOT NULL,
  `inheritance` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cms_nav_permission`
--

INSERT INTO `cms_nav_permission` (`group_id`, `nav_id`, `inheritance`) VALUES
(2, 12, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cms_nav_property`
--

CREATE TABLE `cms_nav_property` (
  `id` int(11) NOT NULL,
  `nav_id` int(11) NOT NULL,
  `admin_prop_id` int(11) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1480446286),
('m141104_104622_admin_group', 1480446293),
('m141104_104631_admin_user_group', 1480446293),
('m141104_114809_admin_user', 1480446293),
('m141203_121042_admin_lang', 1480446294),
('m141203_143052_cms_cat', 1480446294),
('m141203_143059_cms_nav', 1480446294),
('m141203_143111_cms_nav_item', 1480446295),
('m141208_134038_cms_nav_item_page', 1480446295),
('m150106_095003_cms_layout', 1480446295),
('m150108_154017_cms_block', 1480446296),
('m150108_155009_cms_nav_item_page_block_item', 1480446297),
('m150122_125429_cms_nav_item_module', 1480446297),
('m150205_141350_block_group', 1480446297),
('m150304_152220_admin_storage_folder', 1480446298),
('m150304_152238_admin_storage_file', 1480446298),
('m150304_152244_admin_storage_filter', 1480446298),
('m150304_152250_admin_storage_effect', 1480446298),
('m150304_152256_admin_storage_image', 1480446299),
('m150309_142652_admin_storage_filter_chain', 1480446299),
('m150323_125407_admin_auth', 1480446299),
('m150323_132625_admin_group_auth', 1480446300),
('m150331_125022_admin_ngrest_log', 1480446300),
('m150615_094744_admin_user_login', 1480446300),
('m150617_200836_admin_user_online', 1480446301),
('m150626_084948_admin_search_data', 1480446301),
('m150915_081559_admin_config', 1480446302),
('m150922_134558_add_is_offline', 1480446302),
('m150924_112309_cms_nav_prop', 1480446303),
('m150924_120914_admin_prop', 1480446303),
('m151007_084953_storage_folder_is_deleted', 1480446304),
('m151007_113638_admin_file_use_socket', 1480446305),
('m151007_134149_admin_property_class_name', 1480446306),
('m151012_072207_cms_log', 1480446306),
('m151013_132217_login_secure_token', 1480446307),
('m151020_065710_user_force_reload', 1480446307),
('m151022_143429_cms_nav_item_redirect', 1480446307),
('m151026_161841_admin_tag', 1480446309),
('m151028_085932_add_is_home_in_nav', 1480446310),
('m151104_160421_remove_property_fields', 1480446311),
('m151110_113803_rename_rewrite_to_alias', 1480446312),
('m151110_114915_rename_cms_cat_to_cms_nav_container', 1480446313),
('m151116_105124_image_resolution_to_storage_image', 1480446313),
('m151123_114124_add_nav_item_description', 1480446314),
('m151126_090723_add_is_draft_for_nav', 1480446314),
('m151130_075456_block_is_hidden', 1480446314),
('m160329_085913_blockgroupfields', 1480446317),
('m160329_110559_navitemkeywords', 1480446318),
('m160331_075331_pageversiondata', 1480446319),
('m160412_083028_changedcmsproptype', 1480446320),
('m160602_125708_filemanagercaption', 1480446321),
('m160629_092417_cmspermissiontable', 1480446321),
('m160802_140548_add_user_settings_field', 1480446322),
('m160804_082037_cmslog_add_fields', 1480446323),
('m160915_081618_create_admin_logger_table', 1480446323),
('m161024_132116_add_title_tag_field', 1480446324);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_auth`
--
ALTER TABLE `admin_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_config`
--
ALTER TABLE `admin_config`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `admin_group`
--
ALTER TABLE `admin_group`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_lang`
--
ALTER TABLE `admin_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_logger`
--
ALTER TABLE `admin_logger`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_ngrest_log`
--
ALTER TABLE `admin_ngrest_log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_property`
--
ALTER TABLE `admin_property`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `var_name` (`var_name`);

--
-- Indices de la tabla `admin_search_data`
--
ALTER TABLE `admin_search_data`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_storage_effect`
--
ALTER TABLE `admin_storage_effect`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `identifier` (`identifier`);

--
-- Indices de la tabla `admin_storage_file`
--
ALTER TABLE `admin_storage_file`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_storage_filter`
--
ALTER TABLE `admin_storage_filter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `identifier` (`identifier`);

--
-- Indices de la tabla `admin_storage_filter_chain`
--
ALTER TABLE `admin_storage_filter_chain`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_storage_folder`
--
ALTER TABLE `admin_storage_folder`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_storage_image`
--
ALTER TABLE `admin_storage_image`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_tag`
--
ALTER TABLE `admin_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `admin_user_group`
--
ALTER TABLE `admin_user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_user_login`
--
ALTER TABLE `admin_user_login`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_user_online`
--
ALTER TABLE `admin_user_online`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_block`
--
ALTER TABLE `cms_block`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_block_group`
--
ALTER TABLE `cms_block_group`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_layout`
--
ALTER TABLE `cms_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_log`
--
ALTER TABLE `cms_log`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav`
--
ALTER TABLE `cms_nav`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav_container`
--
ALTER TABLE `cms_nav_container`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav_item`
--
ALTER TABLE `cms_nav_item`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav_item_module`
--
ALTER TABLE `cms_nav_item_module`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav_item_page`
--
ALTER TABLE `cms_nav_item_page`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav_item_page_block_item`
--
ALTER TABLE `cms_nav_item_page_block_item`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav_item_redirect`
--
ALTER TABLE `cms_nav_item_redirect`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cms_nav_property`
--
ALTER TABLE `cms_nav_property`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_auth`
--
ALTER TABLE `admin_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `admin_group`
--
ALTER TABLE `admin_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `admin_lang`
--
ALTER TABLE `admin_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `admin_logger`
--
ALTER TABLE `admin_logger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_ngrest_log`
--
ALTER TABLE `admin_ngrest_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `admin_property`
--
ALTER TABLE `admin_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_search_data`
--
ALTER TABLE `admin_search_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_storage_effect`
--
ALTER TABLE `admin_storage_effect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `admin_storage_file`
--
ALTER TABLE `admin_storage_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `admin_storage_filter`
--
ALTER TABLE `admin_storage_filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `admin_storage_filter_chain`
--
ALTER TABLE `admin_storage_filter_chain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `admin_storage_folder`
--
ALTER TABLE `admin_storage_folder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_storage_image`
--
ALTER TABLE `admin_storage_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `admin_tag`
--
ALTER TABLE `admin_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `admin_user_group`
--
ALTER TABLE `admin_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `admin_user_login`
--
ALTER TABLE `admin_user_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `admin_user_online`
--
ALTER TABLE `admin_user_online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `cms_block`
--
ALTER TABLE `cms_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `cms_block_group`
--
ALTER TABLE `cms_block_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `cms_layout`
--
ALTER TABLE `cms_layout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cms_log`
--
ALTER TABLE `cms_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;
--
-- AUTO_INCREMENT de la tabla `cms_nav`
--
ALTER TABLE `cms_nav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `cms_nav_container`
--
ALTER TABLE `cms_nav_container`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `cms_nav_item`
--
ALTER TABLE `cms_nav_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `cms_nav_item_module`
--
ALTER TABLE `cms_nav_item_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cms_nav_item_page`
--
ALTER TABLE `cms_nav_item_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `cms_nav_item_page_block_item`
--
ALTER TABLE `cms_nav_item_page_block_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT de la tabla `cms_nav_item_redirect`
--
ALTER TABLE `cms_nav_item_redirect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cms_nav_property`
--
ALTER TABLE `cms_nav_property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
